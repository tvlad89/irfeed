class pieInterestedParties{
    constructor(labels,
                series,
                htmlid,
                type){
      /*
      type: chart type: year/month... free text
      */
  
      _debugLogger("[!] Creating chart class for: " + type);
      this.htmlid = htmlid;

      /*
      Globals
      */
     this.height = '500px';
     this.width = '100%';
      //Deploy chart
    this.chartSoldier(labels, series);
    }
  
    chartSoldier(labels, series){
      /*
        Build the chart for this object
      */

      _debugLogger("[!] into chartSoldier() - Constructing pie chart.");



      var options = {
        series: series,
        labels: labels,

        fill: { colors: [
          int_parties_table_settings.pie_color_01, 
          int_parties_table_settings.pie_color_02, 
          int_parties_table_settings.pie_color_03, 
          int_parties_table_settings.pie_color_04, 
          int_parties_table_settings.pie_color_05,
          int_parties_table_settings.pie_color_06,
          int_parties_table_settings.pie_color_07,
          int_parties_table_settings.pie_color_08,
          int_parties_table_settings.pie_color_09,
          int_parties_table_settings.pie_color_10,
          int_parties_table_settings.pie_color_11,
          int_parties_table_settings.pie_color_12,
          int_parties_table_settings.pie_color_13,
          int_parties_table_settings.pie_color_14,
          int_parties_table_settings.pie_color_15,
          int_parties_table_settings.pie_color_16,
          int_parties_table_settings.pie_color_17,
          int_parties_table_settings.pie_color_18,
          int_parties_table_settings.pie_color_19,
          int_parties_table_settings.pie_color_20,
          int_parties_table_settings.pie_color_21
        ]},
        colors: [
          int_parties_table_settings.pie_color_01, 
          int_parties_table_settings.pie_color_02, 
          int_parties_table_settings.pie_color_03, 
          int_parties_table_settings.pie_color_04, 
          int_parties_table_settings.pie_color_05,
          int_parties_table_settings.pie_color_06,
          int_parties_table_settings.pie_color_07,
          int_parties_table_settings.pie_color_08,
          int_parties_table_settings.pie_color_09,
          int_parties_table_settings.pie_color_10,
          int_parties_table_settings.pie_color_11,
          int_parties_table_settings.pie_color_12,
          int_parties_table_settings.pie_color_13,
          int_parties_table_settings.pie_color_14,
          int_parties_table_settings.pie_color_15,
          int_parties_table_settings.pie_color_16,
          int_parties_table_settings.pie_color_17,
          int_parties_table_settings.pie_color_18,
          int_parties_table_settings.pie_color_19,
          int_parties_table_settings.pie_color_20,
          int_parties_table_settings.pie_color_21
        ],
        chart: {
            type: 'donut',
            height: this.height,
            width: this.width
        },
        plotOptions: {
            pie: {
              donut: {
                size: '65%',
                labels: {
                  show: true,
                  name: {
                    show: true,
                    fontSize: '20px',
                    fontFamily: 'Helvetica, Arial, sans-serif',
                    fontWeight: 600,
                    color: undefined,
                    offsetY: -10,
                    formatter: function (val) {
                      return val
                    }
                  },
                  value: {
                    show: true,
                    fontSize: '18px',
                    fontFamily: 'Helvetica, Arial, sans-serif',
                    fontWeight: 400,
                    color: undefined,
                    offsetY: 10,
                    formatter: function (val) {
                      return val
                    }
                  }
                }
              }
            }
        },
        legend: {
          show: true,
          showForSingleSeries: false,
          showForNullSeries: true,
          showForZeroSeries: true,
          position: 'bottom',
          horizontalAlign: 'center', 
          floating: false,
          fontSize: '16px',
          fontFamily: 'Helvetica, Arial',
          fontWeight: 400,
          formatter: function(label, opts) {
            return "<div class='first'>" + label + "</div>" + "<div class='second'>" + opts.w.globals.series[opts.seriesIndex] + "%" + "</div>"
        },
          inverseOrder: false,
          width: undefined,
          height: undefined,
          tooltipHoverFormatter: undefined,
          customLegendItems: [],
          offsetX: 0,
          offsetY: 0,
          labels: {
              colors: undefined,
              useSeriesColors: false
          },
          markers: {
              width: 12,
              height: 12,
              strokeWidth: 0,
              strokeColor: '#fff',
              fillColors: undefined,
              radius: 4,
              customHTML: undefined,
              onClick: undefined,
              offsetX: 0,
              offsetY: 0
          },
          itemMargin: {
              horizontal: 10,
              vertical: 10
          },
          onItemClick: {
              toggleDataSeries: true
          },
          onItemHover: {
              highlightDataSeries: true
          },
      }
        
      };

      const pieChartDiv = document.querySelector("div.pieChartDiv");
      if (int_parties_chart_settings.language.hebrew){
        pieChartDiv.setAttribute("dir", "rtl");
      }
      else{
        pieChartDiv.setAttribute("dir", "ltr");
      }

      var chart = new ApexCharts(document.querySelector(this.htmlid), options);
      chart.render();

    }
  }

