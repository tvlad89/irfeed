/*
START Construct charts data
chart building chain:
  1. BuildChart class - pulls data by 1 req for all charts types
  2. navigates to chartConstructor object for each chart type.
*/

class BuildChart {
    constructor(chartType) {
        _debugLogger("[!] into BuildChart class constructor.");
        if (chartType == "pie_interested_parties"){
          this.pullData(_constructAPIURL_interested_parties(), chartType)
        }
        else{
          // Charts: mainChart, miniChart, miniChart2
          this.pullData(_constructAPIURL(), chartType);
        }
        
    }
  
    pullData(url, chartType){
      _debugLogger("[!] into pullData()");
      _debugLogger("chartType: ");
      _debugLogger(chartType);
      fetch(url) // Call the fetch function passing the url of the API as a parameter
      .then((resp) => resp.json()) // Transform the data into json
      .then(function(data) {
        // Your code for handling the data you get from the API
        _debugLogger("[i] Received data from API");
  
        //navigate the data for further operations
        navigate_data(data, chartType)  
      })
      .catch(function(error) {
        document.getElementById("nodedata_buttons").style.display = "none";
        document.getElementById("nodata").innerHTML = _missing_data_text;
        _exception("BuildChart():pullDate(): Failed pulling data from API: " + error);
      });
    }
  }

//data navigation function for BuildChart->pullData fetch
function navigate_data(data, chartType){
    _debugLogger("[i] Navigating data to chartConstructor object - obj for each chart type.");
    var constructMainChart = new chartConstructor(data, 'main', chartType);
}