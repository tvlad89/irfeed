//Cunstruct the data for each chart
class chartConstructor{
    constructor(data, type, chartType) {
        _debugLogger("[!] Into chartConstructor class for: " + type);
        this.data = data;
        this.type = type;   //chart type: weekly, year, month...

        /*
        Interval to show data for specific charts
        */
        this.week_minutes_interval = 15;
        this.day_minutes_interval = 4;

        /*
        BUG FIX: Daily graph timezone - add +3h
        */
        this.shift_timezone_fix = 3;

        /*
        Limit the day graph with specific hour. 
        */
        this.x_axis_day_limit_hour = 20 + this.shift_timezone_fix;

        this.pie_labels_arr_interested_parties = new Array();
        this.pie_series_arr_interested_parties = new Array();

        /*
        Actions
        */

        if (chartType == 'miniChart'){
            this.buildMiniChart();
        }
        else if(chartType == 'miniChart2'){
            this.buildMiniChart2();
        }
        else if(chartType == 'mainChart'){
            this.buildDataArrays();
        }
        else if(chartType == 'pie_interested_parties'){
            this.buildPie_interested_parties();
        }
        else{
            _debugLogger('[!] chartConstructor message: chartType wrong / not set.');
        }
        
    }

    buildPie_interested_parties(){
        /*
        Build interested_parties dataset
        - TODO: 
        */
        _debugLogger(this.data);
        _debugLogger("WORKING FROM HERE :)");
        this.pie_build_series_interested_parties(this.data);
        this.apply_pie_interested_parties('#pie_interested_parties');
    }

    pie_build_series_interested_parties(obj){
        /*
        Pie chart data series
        */

        //Init array
        var pie_series_arr = new Array();
        var dynamic_key = Object.keys(obj)[0];   // data starting with some dynamic key, like: 475 - we dont know it.
        var data_length = Object.keys(obj[dynamic_key]).length;

        //Build data array - create array for FortunePrecent - only when bigger than 0.
        var public_hold = 100 // Substract the fortune holders percent to get the public hold
        for(var i=0; i < data_length; i++){
            if (obj[dynamic_key][i].FortunePrecent > 0){
                public_hold = public_hold - obj[dynamic_key][i].FortunePrecent;
                this.pie_series_arr_interested_parties.push(parseFloat(obj[dynamic_key][i].FortunePrecent));
                this.pie_labels_arr_interested_parties.push(obj[dynamic_key][i].IntrestedPartyName)
            }
        }
        this.pie_series_arr_interested_parties.push(parseFloat(public_hold.toFixed(2)));
        this.pie_labels_arr_interested_parties.push('החזקות ציבור')

        //log
        _debugLogger('series dataset: ');
        _debugLogger(this.pie_series_arr_interested_parties);
        _debugLogger('labels dataset: ');
        _debugLogger(this.pie_labels_arr_interested_parties);
    }

    buildMiniChart(){
        /*
        Build mini chart datasets
        */

        _debugLogger(this.data);
        //var minichart_x = this.build_x_axis(this.data.three_month, '3month');  //used for miniChart
        //var minichart_y = this.build_y_axis(this.data.three_month, '3month');  //used for miniChart
        var minichart_x = this.build_x_axis_day(this.data.one_day, '1day');
        var minichart_y = this.build_y_axis_day(this.data.one_day, '1day');
        var miniChart_realtime_annotation = this.build_last_day_dataset_timestamp_annotation(minichart_x)
        var y_max_minichart = _get_max_array(minichart_y);
        var y_min_minichart = _get_min_array(minichart_y);
        var day_x_axis_limit = this.build_last_day_dataset_timestamp_with_shift(this.data.one_day, this.x_axis_day_limit_hour)

        this.applyMiniChart(minichart_y,minichart_x,y_max_minichart,y_min_minichart,miniChart_realtime_annotation,day_x_axis_limit,'#miniChart');
    }

    buildMiniChart2(){
        /*
        Build mini chart 2 datasets
        */

        _debugLogger(this.data);
        var minichart_x = this.build_x_axis(this.data.one_month, '1month');
        var minichart_y = this.build_y_axis(this.data.one_month, '1month');

        var y_max_minichart = _get_max_array(minichart_y);
        var y_min_minichart = _get_min_array(minichart_y);

        this.applyMiniChart2(minichart_y,minichart_x,y_max_minichart,y_min_minichart,'#miniChart2');
    }
  
    buildDataArrays(){
        /*
        For each dataset, convert the json data into arrays of dates & relevant fields.
        */

        _debugLogger('[!] All Data: ');
        _debugLogger(this.data);

        _debugLogger('[!] Building x/y axis for graphs: ');
        //var year_x = this.build_x_axis(this.data.one_year, 'year');
        var year_y = this.build_y_axis(this.data.one_year, 'year');
        var y_max_year = _get_max_array(year_y);
        var y_min_year = _get_min_array(year_y);
        _debugLogger('[!] one year y max : ' + y_max_year);
        _debugLogger('[!] one year y min : ' + y_min_year); 

        //var threeyear_x = this.build_x_axis(this.data.three_year, '3year');
        var threeyear_y = this.build_y_axis(this.data.three_year, '3year');
        var y_max_threeyear = _get_max_array(threeyear_y);
        var y_min_threeyear = _get_min_array(threeyear_y);
        _debugLogger('[!] three year y max : ' + y_max_threeyear);
        _debugLogger('[!] three year y min : ' + y_min_threeyear);        

        var fiveyear_x = this.build_x_axis(this.data.five_year, '5year');
        var fiveyear_y = this.build_y_axis(this.data.five_year, '5year');
        var y_max_fiveyear = _get_max_array(fiveyear_y);
        var y_min_fiveyear = _get_min_array(fiveyear_y);
        _debugLogger('[!] five year y max : ' + y_max_fiveyear);
        _debugLogger('[!] five year y min : ' + y_min_fiveyear);
  
        //var oneweek_x = this.build_x_axis(this.data.one_week, '1week');
        var onemonth_y = this.build_y_axis(this.data.one_month, '1month');
        var y_max_onemonth = _get_max_array(onemonth_y);
        var y_min_onemonth = _get_min_array(onemonth_y);
        _debugLogger('[!] one month y max : ' + y_max_onemonth);
        _debugLogger('[!] one month y min : ' + y_min_onemonth);


        //var sixmonth_x = this.build_x_axis(this.data.six_month, '6month');
        var sixmonth_y = this.build_y_axis(this.data.six_month, '6month');
        var y_max_sixmonth = _get_max_array(sixmonth_y);
        var y_min_sixmonth = _get_min_array(sixmonth_y);
        _debugLogger('[!] six month y max : ' + y_max_sixmonth);
        _debugLogger('[!] six month y min : ' + y_min_sixmonth);

        var threemonth_x = this.build_x_axis(this.data.three_month, '3month');  //used for miniChart
        var threemonth_y = this.build_y_axis(this.data.three_month, '3month');  //used for miniChart
        var y_max_threemonth = _get_max_array(threemonth_y);
        var y_min_threemonth = _get_min_array(threemonth_y);
        _debugLogger('[!] three month y max : ' + y_max_threemonth);
        _debugLogger('[!] three month y min : ' + y_min_threemonth);

        /*
        1week: Data generated from db - week details for the last 7 days - BY HOUR - different X axis.
        */

        var oneweek_x = this.build_x_axis_week(this.data.one_week, '1week', 'xaxis');
        var oneweek_x_tooltip = this.build_x_axis_week(this.data.one_week, '1week', 'tooltip');
        var oneweek_y = this.build_y_axis_week(this.data.one_week, '1week');   
        var y_max_oneweek = _get_max_array(oneweek_y);
        var y_min_oneweek = _get_min_array(oneweek_y);
        _debugLogger('[!] one week y max : ' + y_max_oneweek);
        _debugLogger('[!] one week y min : ' + y_min_oneweek);

        /*
        1day realtime data - generated in db each 1m - In 1day the X axis is different ! By minute - not by day as others.
        */
        var oneday_x = this.build_x_axis_day(this.data.one_day, '1day');
        var oneday_y = this.build_y_axis_day(this.data.one_day, '1day');
        var y_max_oneday = _get_max_array(oneday_y);
        var y_min_oneday = _get_min_array(oneday_y);
        _debugLogger('[!] One day y max : ' + y_max_oneday);
        _debugLogger('[!] One day y min : ' + y_min_oneday);

        /*
        Tooltip data - pull once for the max range that the chart is built for - 5 year
        */
        var five_year_daily_high = this.build_daily_high_arr(this.data.five_year, '5year');;
        var five_year_daily_low = this.build_daily_low_arr(this.data.five_year, '5year');;
        var five_year_opening_rate = this.build_opening_rate_arr(this.data.five_year, '5year');;

        /*
        Get the first timestamp - to fix x axis missing values gap - help building it dynamically
        */
        var last_timestamp = this.build_today_timestamp(this.data.five_year);

        /*
        Week/Day xaxis limit
        Annotation - last X axis time value
        */
        var day_x_axis_limit = this.build_last_day_dataset_timestamp_with_shift(this.data.one_day, this.x_axis_day_limit_hour)
        var day_x_axis_annotation = this.build_last_day_dataset_timestamp_annotation(oneday_x)
        var day_y_axis_annotation = this.build_last_day_dataset_base_annotation(oneday_y)
        var week_x_axis_annotation = this.build_last_day_dataset_timestamp_annotation(oneweek_x)

        this.applyCharts(fiveyear_y, 
            fiveyear_x, 
            y_max_year, 
            y_min_year, 
            y_max_threeyear, 
            y_min_threeyear, 
            y_max_fiveyear, 
            y_min_fiveyear, 
            y_max_oneweek, 
            y_min_oneweek, 
            y_max_sixmonth, 
            y_min_sixmonth, 
            y_max_threemonth, 
            y_min_threemonth,
            y_max_onemonth,
            y_min_onemonth,
            oneday_x,
            y_max_oneday,
            y_min_oneday,
            oneweek_x,
            oneweek_y,
            oneday_y,
            five_year_daily_high,
            five_year_daily_low,
            five_year_opening_rate,
            last_timestamp,
            day_x_axis_limit,
            day_x_axis_annotation,
            week_x_axis_annotation,
            oneweek_x_tooltip,
            threemonth_x,
            threemonth_y,
            day_y_axis_annotation);
    }

    build_x_axis(obj, type){
        /*
        Array data X
        */
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - X size: ' + daysNum);

        //Print data for specific object once
        //this.print_obj_contecnt(obj, type, 'X & Y');

        //Init array
        var x_data_call_strdate = new Array();
        
        //Build data arrays
        for(var i=0; i < daysNum; i++){
            var call_strdate = obj[i].call_date;  //dates array
            x_data_call_strdate.push(call_strdate);
        }

        this.print_obj_contecnt(x_data_call_strdate, type, 'X');
        return(x_data_call_strdate);
    }

    build_x_axis_week(obj, type, arr_type){
        /*
        Array data X - week
        */
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - X size: ' + daysNum);

        
        //this.print_obj_contecnt(obj, type, 'X & Y');  //Print data for specific object once

        var x_data_call_strdate = new Array();
        var x_data_call_strdate_tooltip = new Array();
        
        /*
        Build data arrays
        Initialize default interval for data: this.week_minutes_interval
        */
        var minutes_interval = 0;
        for(var i=0; i < daysNum; i++){
             if (minutes_interval == this.week_minutes_interval){

                var call_strdate = obj[i].call_date_time;  //dates array

                if (main_chart_settings.language.hebrew){
                    //var months = ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'];
                    var months = ['ינו', 'פבר', 'מרץ', 'אפר', 'מאי', 'יונ', 'יול', 'אוג', 'ספט', 'אוק', 'נוב', 'דצמ'];
                }
                else{
                    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                }
                var date = new Date(call_strdate * 1000);   //Unix to datetime
                var year = date.getFullYear();
                var month = months[date.getMonth()];
                var day = date.getDate();
                var hours = date.getHours();
    
                var minutes = date.getMinutes();
                if (minutes<10){
                    minutes = "0" + minutes
                }
                var time_tooltip = day + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes;
                var time_x = day + ' ' + month;
                x_data_call_strdate.push(time_x);
                x_data_call_strdate_tooltip.push(time_tooltip);

                minutes_interval = 0;
            }
            else{
                minutes_interval = minutes_interval + 1;
            }
        }

        this.print_obj_contecnt(x_data_call_strdate, type, 'X');

        var returning = 'no_data';
        if(arr_type == 'tooltip'){
            returning = x_data_call_strdate_tooltip;
        }
        else if(arr_type == 'xaxis'){
            returning = x_data_call_strdate;
        }

        return(returning);
    }

    build_x_axis_day(obj, type){
        /*
        Array data X - day
        */
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - X size: ' + daysNum);

        
        //this.print_obj_contecnt(obj, type, 'X & Y');  //Print data for specific object once

        var x_data_call_strdate = new Array();
        
        /*
        Build data arrays
        Initialize default interval for data: this.day_minutes_interval
        */
        var minutes_interval = 0;
         
        for(var i=0; i < daysNum; i++){
             if (minutes_interval == this.day_minutes_interval){

                var call_strdate = obj[i].call_date_time;  //dates array

                var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                var date = new Date(call_strdate * 1000);   //Unix to datetime
                var year = date.getFullYear();
                var month = months[date.getMonth()];
                var day = date.getDate();
                var hours = date.getHours() + this.shift_timezone_fix;
    
                var minutes = date.getMinutes();
                if (minutes<10){
                    minutes = "0" + minutes
                }

                var time = day + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes;
                //var time = hours + ':' + minutes;
                x_data_call_strdate.push(time);

                minutes_interval = 0;
            }
            else{
                minutes_interval = minutes_interval + 1;
            }
        }
        

        this.print_obj_contecnt(x_data_call_strdate, type, 'X');
        return(x_data_call_strdate);
    }

    build_y_axis(obj, type){
        /*
        Array data Y
        */
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - Y size: ' + daysNum);

        var y_data_lock_rate = new Array(); //Init array
        
        for(var i=0; i < daysNum; i++){
            var lock_rate = obj[i].lock_rate;
            y_data_lock_rate.push(parseInt(lock_rate).toFixed(0));
        }
        this.print_obj_contecnt(y_data_lock_rate, type, 'Y');
        return(y_data_lock_rate);
    }

    /*
    TODO - Remove - missing lock_rate, using daily_low value for tests
    */
    build_y_axis_week(obj, type){
        /*
        Array data Y - Week
        */
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - Y size: ' + daysNum);

        var y_data_lock_rate = new Array(); //Init array
        
        var minutes_interval = 0;
        for(var i=0; i < daysNum; i++){
            if (minutes_interval == this.week_minutes_interval){
            var lock_rate = obj[i].LastKnownRate;
            lock_rate = lock_rate.replace(',','');
            y_data_lock_rate.push(parseInt(lock_rate).toFixed(0));
            minutes_interval=0;
            }
            else{
                minutes_interval = minutes_interval + 1;
            }
        }
        
        this.print_obj_contecnt(y_data_lock_rate, type, 'Y');
        return(y_data_lock_rate);
    }

    /*
    TODO: change BaseRate value to normal one :)
    */
    build_y_axis_day(obj, type){
        /*
        Array data Y - Day
        */
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - Y size: ' + daysNum);

        var y_data_lock_rate = new Array(); //Init array
        
        var minutes_interval = 0;
        for(var i=0; i < daysNum; i++){
            if (minutes_interval == this.day_minutes_interval){
            var lock_rate = obj[i].LastKnownRate;
            lock_rate = lock_rate.replace(',','');
            y_data_lock_rate.push(parseInt(lock_rate).toFixed(1));
            minutes_interval=0;
            }
            else{
                minutes_interval = minutes_interval + 1;
            }
        }

        this.print_obj_contecnt(y_data_lock_rate, type, 'Y');
        return(y_data_lock_rate);
    }

    build_opening_rate_arr(obj, type){

        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - opening_rate: ' + daysNum);

        var y_data_opening_rate = new Array();   //Init array
        
        for(var i=0; i < daysNum; i++){
            var opening_rate = obj[i].opening_rate;
            y_data_opening_rate.push(parseInt(opening_rate).toFixed(0));
        }
        this.print_obj_contecnt(y_data_opening_rate, type, 'opening_rate');
        return(y_data_opening_rate);
    }

    build_daily_high_arr(obj, type){
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - daily_high: ' + daysNum);

        var y_data_daily_high = new Array();
        
        for(var i=0; i < daysNum; i++){
            var daily_high = obj[i].daily_high;
            y_data_daily_high.push(parseInt(daily_high).toFixed(0));
        }
        this.print_obj_contecnt(y_data_daily_high, type, 'daily_high');
        return(y_data_daily_high);
    }

    build_daily_low_arr(obj, type){
        var daysNum = _objectSize(obj);
        _debugLogger('[!] Object - ' + type + ' - daily_low: ' + daysNum);

        var y_data_daily_low = new Array();
        
        for(var i=0; i < daysNum; i++){
            var daily_low = obj[i].daily_low;
            y_data_daily_low.push(parseInt(daily_low).toFixed(0));
        }
        this.print_obj_contecnt(y_data_daily_low, type, 'daily_low');
        return(y_data_daily_low);
    }

    build_today_timestamp(obj){
        /*
        used to fix today value missing gap - on x axis
        */
        _debugLogger('[!] Into build_today_timestamp');
        _debugLogger('[!] build_today_timestamp (date): First object: ');
        _debugLogger(obj[0].call_date);
        return(obj[0].call_date);
    }

    build_last_day_dataset_timestamp_with_shift(obj, limit){
        /*
        used to fix today value missing gap - on x axis
        */
        _debugLogger('[!] Into build_last_day_dataset_timestamp_with_shift');
        _debugLogger('[!] build_last_day_dataset_timestamp_with_shift (date): First object: ');
        _debugLogger(obj[0].call_date);
        

        var last_Unix_time_in_set = obj[0].call_date_time;  //dates array

        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var date = new Date(last_Unix_time_in_set * 1000);   //Unix to datetime
        var year = date.getFullYear();
        var month = months[date.getMonth()];
        var day = date.getDate();

        /*
        this.shift_xaxis_right_time - used to set the limit in x axis - for example - 19:00 .
        */
        var hours = limit;
        var minutes = '00';
        var time = day + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes;

        _debugLogger('[!] build_last_day_dataset_timestamp_with_shift: returned value: ');
        _debugLogger(time);
        return(time);

    }

    build_last_day_dataset_timestamp_annotation(obj){
        /*
        Realtime x axis value annotation
        */

        var len = _objectSize(obj);
        var time = obj[len-1];
        _debugLogger('[!] build_last_day_dataset_timestamp_annotation (date): annotation x axis value: ' + time);
        return(time);

    }

    build_last_day_dataset_base_annotation(obj){
        /*
        1 day chart - base line annotation - starting value.
        */
       var len = _objectSize(obj);
       var val = obj[0];
       _debugLogger('[!] build_last_day_dataset_base_annotation : annotation y axis base value: ' + val);
       return(val);
    }


    print_obj_contecnt(obj, type, axis){
        _debugLogger('[!] Object content for - ' + type + ' - ' + axis + ': ');
        _debugLogger(obj);
    }

    applyCharts(fiveyear_y, 
        fiveyear_x, 
        y_max_year, 
        y_min_year, 
        y_max_threeyear, 
        y_min_threeyear, 
        y_max_fiveyear, 
        y_min_fiveyear, 
        y_max_oneweek, 
        y_min_oneweek, 
        y_max_sixmonth, 
        y_min_sixmonth, 
        y_max_threemonth, 
        y_min_threemonth, 
        y_max_onemonth, 
        y_min_onemonth,
        oneday_x,
        y_max_oneday,
        y_min_oneday,
        oneweek_x,
        oneweek_y,
        oneday_y,
        five_year_daily_high,
        five_year_daily_low,
        five_year_opening_rate,
        last_timestamp,
        day_x_axis_limit,
        day_x_axis_annotation,
        week_x_axis_annotation,
        oneweek_x_tooltip,
        threemonth_x,
        threemonth_y,
        day_y_axis_annotation){
        //Apply all data on charts - create charts objects
        
        
        var fiveYearChartObject = new chartSetup(
            fiveyear_y.reverse(), 
            fiveyear_x.reverse(),
            y_max_year, 
            y_min_year, 
            y_max_threeyear, 
            y_min_threeyear, 
            y_max_fiveyear, 
            y_min_fiveyear, 
            y_max_sixmonth, 
            y_min_sixmonth, 
            y_max_threemonth, 
            y_min_threemonth,
            y_max_onemonth,
            y_min_onemonth,
            five_year_daily_high.reverse(),
            five_year_daily_low.reverse(),
            five_year_opening_rate.reverse(),
            last_timestamp,
            "#fiveYearChart", 
            '5Year',
            'MMM yy');
        
        var weekChartObject = new chartSetupWeek(
            oneweek_y, 
            oneweek_x,
            "#weekChart", 
            '1week',
            'dd MMM',
            day_x_axis_limit,
            week_x_axis_annotation,
            oneweek_x_tooltip,
            y_max_oneweek, 
            y_min_oneweek);

        
        var dayChartObject = new chartSetupDay(
            oneday_y.reverse(), 
            oneday_x.reverse(),
            "#dayChart", 
            '1day',
            'HH',
            day_x_axis_limit,
            day_x_axis_annotation,
            day_y_axis_annotation,
            y_max_oneday,
            y_min_oneday);

        _debugLogger('[!] Finished charts setup. Time: ' + Date(Date.now()).toString());
     }

     applyMiniChart(minichart_y,
                    minichart_x,
                    y_max_minichart,
                    y_min_minichart,
                    miniChart_realtime_annotation,
                    day_x_axis_limit,
                    htmlid
                    ){

        var miniChartObject = new miniChart(
            minichart_y,
            minichart_x,
            y_max_minichart,
            y_min_minichart,
            miniChart_realtime_annotation,
            day_x_axis_limit,
            htmlid,
            'mini Chart'
        );
        
        _debugLogger('[!] Finished mini chart setup. Time: ' + Date(Date.now()).toString());
     }

     applyMiniChart2(minichart_y,
        minichart_x,
        y_max_minichart,
        y_min_minichart,
        htmlid
        ){

        var miniChartObject2 = new miniChart2(
        minichart_y,
        minichart_x,
        y_max_minichart,
        y_min_minichart,
        htmlid,
        'mini Chart 2'
        );

        _debugLogger('[!] Finished mini chart 2 setup. Time: ' + Date(Date.now()).toString());
    }

    apply_pie_interested_parties(htmlid){
        _debugLogger('[!] Into apply_pie_interested_parties')
        var pie_interested_parties_obj = new pieInterestedParties(
                                            this.pie_labels_arr_interested_parties,
                                            this.pie_series_arr_interested_parties,
                                            htmlid,
                                            'pie_interested_parties'
                                            );

        _debugLogger('[!] Finished pie_interested_parties setup. Time: ' + Date(Date.now()).toString());

        build_initerested_parties_table(this.pie_labels_arr_interested_parties, this.pie_series_arr_interested_parties);
        _debugLogger('[!] Finished build_initerested_parties_table()');
    }
    
  }