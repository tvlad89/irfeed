/*
General purpose functions library
*/

function _constructAPIURL(){
    _debugLogger("[i] into _constructAPIURL()");
    var _api_url = _api_domain + _api_endpoint + _constructAPIQUERY();
    return _api_url;
  }

function _constructAPIURL_interested_parties(){
    _debugLogger("[i] into _constructAPIURL()");
    // https://finance.irfeed.co.il/api/getInterestedParties?api_key=adc533dd78c08253bf41&stock_number[0]=475020
    var _api_url = _api_domain + _api_endpoint_interested_parties + _constructAPIQUERY_interested_parties();
    return _api_url;
  }
  
function _constructAPIQUERY_interested_parties(){
  _debugLogger("[i] into _constructAPIQUERY()");
  var _api_query = "?stock_number[0]=" + _api_stock_number  + "&api_key=" + _api_key;
  return _api_query;
}

function _constructAPIQUERY(){
  _debugLogger("[i] into _constructAPIQUERY()");
  //var _api_query = "?stock_number[0]=" + _api_stock_number  + "&api_key=" + _api_key + "&from_date=" + from_date + "&to_date=" + to_date;
  var _api_query = "?stock_number[0]=" + _api_stock_number  + "&api_key=" + _api_key;
  return _api_query;
}

function _debugLogger(str){
  //log debug data to console - using only in dev mode (set in globals)
  if(env == 'dev' || env == 'wordpress'){
    console.log(str);
  }
}

//Log exceptions & alert!
function _exception(ex){
  _debugLogger("[ERR] _exception:(): " + ex);
  throw new Error(ex);
}

function _objectSize(obj){
  var size = Object.keys(obj).length;
  return size;
}

function _generate_date_YEARS_back(date, shift){
  var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var d = date.getDate();
  //var m = date.getMonth() + 1; //Month from 0 to 11 options
  var m = strArray[date.getMonth()];
  var y = date.getFullYear() - shift;
  return '' + (d <= 9 ? '0' + d : d) + ' ' + (m<=9 ? '0' + m : m) + ' ' + y;
}


function _generate_date_hours_forward(date, shift){
  /*
  Will work only till 24PM ! No logic for next day. Needed to make the graph look better on week/day.
  */
  var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var d = date.getDate();
  //var m = date.getMonth() + 1; //Month from 0 to 11 options
  var m = strArray[date.getMonth()];
  var y = date.getFullYear();
  var hours = date.getHours() + shift
    
  var minutes = date.getMinutes();
  if (minutes<10){
      minutes = "0" + minutes
  }
  return '' + (d <= 9 ? '0' + d : d) + ' ' + (m<=9 ? '0' + m : m) + ' ' + y + ' ' + hours + ':' + minutes;
}

function _generate_date_MONTHS_back(date, shift){
  var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var d = date.getDate();
  var y = date.getFullYear();
  var month_calc = date.getMonth() - shift;
  if(month_calc<0){
    month_calc=12+month_calc; //+ because month_calc is (-)
    y = date.getFullYear() - 1; //if month was (-), so year is also should be decreased.
  }
  var m = strArray[month_calc];
  
  return '' + (d <= 9 ? '0' + d : d) + ' ' + (m<=9 ? '0' + m : m) + ' ' + y;
}

function _generate_date_DAYS_back(date, shift){
  var strArray_2=['דצמ', 'נוב' ,'אוקט', 'ספט', 'אוג', 'יולי', 'יוני', 'מאי', 'אפר', 'מרץ', 'פב', 'ינ'];
  _debugLogger("[!] _generate_date_DAYS_back() - heb arr: " + strArray_2)

  if (main_chart_settings.language.hebrew){
    var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  }
  else{
    var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  }
  
  _debugLogger("[!] _generate_date_DAYS_back() - for: " + date + ' ,shift: ' + shift)

  var x = date.getDate();  //get data actual day now = x
  var month_calc = date.getMonth(); // get data acual month now = month_calc

  var z = date.setDate(date.getDate() - shift); //shift days back = b
  var c = date.getDate() - shift;
  var b = date.getDate();
    
  var y = date.getFullYear();

  _debugLogger("[i] _generate_date_DAYS_back(): data actual day now: x= " + x);
  _debugLogger("[i] _generate_date_DAYS_back(): data acual month now : month_calc= " + month_calc);
  _debugLogger("[i] _generate_date_DAYS_back(): shifted " + shift + " days back day: b= " + b);

  if(shift >= x){
    //substract one month if days substract greater then current day
    month_calc = month_calc - 1; // will show less by 1 because starting from 0
    _debugLogger("[i] _generate_date_DAYS_back(): !substract! month: x=" + x + ' , shift=' + shift + ' new month val: ' + month_calc);
  }
  _debugLogger("[i] _generate_date_DAYS_back(): Final before year check: x=" + x + ' , shift=' + shift + ' new month val: ' + month_calc);
  if(month_calc<0){
    //fix year if month is (-)
    month_calc=12+month_calc; //+ because month_calc is (-)
    y = y - 1; //if month was (-), so year is also should be decreased.
    _debugLogger("[i] _generate_date_DAYS_back(): !substract! year: month_calc=" + month_calc + ' new year: ' + y);
  }

  var m = strArray[month_calc];
  
  return '' + (b <= 9 ? '0' + b : b) + ' ' + (m<=9 ? '0' + m : m) + ' ' + y;
}

//Get max num from Strings array
function _get_max_array(arr){
  //_debugLogger("[i] into _get_max_array");
  //_debugLogger("[i] Arr size: " + _objectSize(arr))
  var i = 0;
  var maxNum = parseInt(arr[0]);
  for(i=0;i<_objectSize(arr);i++){
    var item = parseInt(arr[i]);
    //DEBUG
    //_debugLogger(i + ". " + arr[i]);
    if(item > maxNum){
      maxNum = item;
    }
  }
  return(maxNum);
}

//Get min num from Strings array
function _get_min_array(arr){
  //_debugLogger("[i] into _get_min_array");
  //_debugLogger("[i] Arr size: " + _objectSize(arr))
  var i = 0;
  var minNum = parseInt(arr[0]);
  for(i=0;i<_objectSize(arr);i++){
    var item = parseInt(arr[i]);
    //_debugLogger(i + ". " + arr[i]);
    if(item < minNum){
      minNum = item;
    }
  }
  return(minNum);
}

function _num_after_dot_convert_array(arr){ //not used
  var x = 0;
  var len = arr.length
  while(x < len){ 
    arr[x] = parseInt(arr[x]);
    arr[x] = arr[x].toFixed(1); 
      x++
  }
  return arr;
}

function _change_rate_percent_html_tag(point, prev_point){
  /*
  Calculate change % from previous value, return html span with color - red=down change, green=rising change.
  */
  var prs = '0';
  var return_text = '';
  if (prev_point){
    prs = (((point*100)/(prev_point)) - 100).toFixed(2);
  }
  var color_tag = 'style="color:green"';
  if (prs<0){
    color_tag = 'style="color:red"';
  }
  prs += '%';
  if (main_chart_settings.language.hebrew){
    var return_text = '<span> ' + main_chart_settings.info_box.version_hebrew.change_txt + ' </span>' + '<span ' + color_tag + '>' + prs + '</span>' + '<br>';
  }
  else{
    var return_text = '<span> ' + main_chart_settings.info_box.version_english.change_txt + ' </span>' + '<span ' + color_tag + '>' + prs + '</span>' + '<br>';
  }
  
  return return_text;
}


function build_initerested_parties_table(labels, series){
  /*
  Build the interested parties dynamic table
  implemented by: chartConstructor.js -> apply_pie_interested_parties()
  */

  interested_parties_table_create_table()
  labels_length = labels.length;
  series_length = series.length;
  const data_object = {};
  for(i=0;i<labels_length;i++){
    data_object[i] = {};
    data_object[i]['label'] = labels[i];
    data_object[i]['value'] = series[i];
  }
  for(i=0;i<labels_length;i++){
    interested_parties_table_append_values_row(data_object[i]['label'], data_object[i]['value']);
  }

}

function interested_parties_table_create_table() {
  _debugLogger("[!] Table: Creating");
    
  const tableDiv = document.querySelector("div.TableDiv");

  if (int_parties_table_settings.language.hebrew){
    tableDiv.setAttribute("dir", "rtl");
    tableHeaders = int_parties_table_settings.tableHeaders_heb;
  }
  else{
    tableDiv.setAttribute("dir", "ltr");
    tableHeaders = int_parties_table_settings.tableHeaders_eng;
  }
  

  while (tableDiv.firstChild) tableDiv.removeChild(tableDiv.firstChild)
  
  let mainTable = document.createElement('table');
  mainTable.className = 'mainTable';
  
  let mainTableHead = document.createElement('thead');
  mainTableHead.className = 'mainTableHead';
  
  let mainTableHeaderRow = document.createElement('tr');
  mainTableHeaderRow.className = 'mainTableHeaderRow';
  
  tableHeaders.forEach(header => {
    let mainHeader = document.createElement('th');
    mainHeader.innerText = header;
    mainTableHeaderRow.append(mainHeader);
  })
    
  mainTableHead.append(mainTableHeaderRow);
  mainTable.append(mainTableHead);
  let mainTableBody = document.createElement('tbody');
  mainTableBody.className = "mainTable-Body";
  mainTable.append(mainTableBody);
  tableDiv.append(mainTable);
}

function interested_parties_table_append_values_row(label, value) {
  _debugLogger("[!] Table: appending label + value");
  const mainTable = document.querySelector('.mainTable');
  let mainTableBodyRow = document.createElement('tr');
  mainTableBodyRow.className = 'mainTableBodyRow';
  
  let labelRow = document.createElement('td');
  labelRow.innerText = label;
  let valueRow = document.createElement('td');
  valueRow.innerText = value;

  mainTableBodyRow.append(labelRow, valueRow);
  mainTable.append(mainTableBodyRow);
}