class chartSetup{
  constructor(y_data, 
              x_data,
              y_max_year, 
              y_min_year, 
              y_max_threeyear, 
              y_min_threeyear, 
              y_max_fiveyear, 
              y_min_fiveyear, 
              y_max_sixmonth, 
              y_min_sixmonth, 
              y_max_threemonth, 
              y_min_threemonth,
              y_max_onemonth,
              y_min_onemonth,
              five_year_daily_high,
              five_year_daily_low,
              five_year_opening_rate,
              last_timestamp,
              htmlid, 
              type, 
              date_format){
    /*
    y_data: the y data passed as array
    x_data: the x data passed as array
    htmlid: the ID where to implement the chart
    type: chart type: year/month... free text
    date_format: in which format show the dates
    max/min values: used for fitting Y axis dynamically
    */

    _debugLogger("[!] Creating chartSetup class for: " + type);
    this.ydata = y_data;
    this.xdata = x_data;
    this.htmlid = htmlid;
    this.type = type;
    this.date_format = date_format;

    

    /*
    Globals:
    */
   this.y_scale_shift = _y_axis_shift; //Scale the dynamic y borders - down/up


    //Deploy chart
    this.chartSoldier(y_max_year, 
                      y_min_year, 
                      y_max_threeyear, 
                      y_min_threeyear, 
                      y_max_fiveyear, 
                      y_min_fiveyear, 
                      y_max_sixmonth, 
                      y_min_sixmonth, 
                      y_max_threemonth, 
                      y_min_threemonth,
                      y_max_onemonth,
                      y_min_onemonth,
                      five_year_daily_high,
                      five_year_daily_low,
                      five_year_opening_rate,
                      last_timestamp);
  }

  chartSoldier(y_max_year, 
                y_min_year, 
                y_max_threeyear, 
                y_min_threeyear, 
                y_max_fiveyear, 
                y_min_fiveyear, 
                y_max_sixmonth, 
                y_min_sixmonth, 
                y_max_threemonth, 
                y_min_threemonth,
                y_max_onemonth,
                y_min_onemonth,
                five_year_daily_high,
                five_year_daily_low,
                five_year_opening_rate,
                last_timestamp){
    //Build chart for this object
    var y_scale_shift = this.y_scale_shift;
    
    
    _debugLogger("[!] into chartSoldier()");
    
    //For default chart - 1Y
    var dateShifted = _generate_date_YEARS_back(new Date(), 1);
    var info_box_txt = '';
    if (main_chart_settings.language.hebrew){
      var tool_box_offset_x = main_chart_settings.info_box.version_hebrew.offsetX;
      var tool_box_position = main_chart_settings.info_box.version_hebrew.position;
      var default_locale = 'heb';
    }
    else{
      var tool_box_offset_x = main_chart_settings.info_box.version_english.offsetX;
      var tool_box_position = main_chart_settings.info_box.version_english.position;
      var default_locale = 'en';
    }
    
    
    //Chart options & configs
    var options = {
      series: [{
        //name: 'lock rate',
        data: this.ydata,
      },
      ],
      chart: {
        id: 'chart1',
        type: _chart_type,
        width:'100%',
        height: _chart_height,
        toolbar: {
          tools: {
          zoom: false,
          zoomin: '<button class="btn-ideadice-toolbar"><i class="fas fa-plus"></i></i></button>',
          zoomout: '<button class="btn-ideadice-toolbar"><i class="fas fa-minus"></i></button>',
          pan: '<button class="btn-ideadice-toolbar"><i class="fas fa-ellipsis-h"></i></button>',
          download: '<button class="btn-ideadice-toolbar"><i class="fas fa-cloud-download-alt"></i></button>',
          reset: false,
          },
        },
        offsetX: 0,
        defaultLocale: default_locale,
        locales: [{
          name: 'en',
          options: {
            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            toolbar: {
              download: 'Download SVG',
              selection: 'Selection',
              selectionZoom: 'Selection Zoom',
              zoomIn: 'Zoom In',
              zoomOut: 'Zoom Out',
              pan: 'Panning',
              reset: 'Reset Zoom',
            }
          }
        },
        {
          name: 'heb',
          options: {
            months: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'],
            shortMonths: ['ינו', 'פבר', 'מרץ', 'אפר', 'מאי', 'יונ', 'יול', 'אוג', 'ספט', 'אוק', 'נוב', 'דצמ'],
            days: ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'],
            shortDays: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
            toolbar: {
              download: 'הורדה',
              selection: 'סימון',
              selectionZoom: 'הגדלת סימון',
              zoomIn: 'הקרב',
              zoomOut: 'הרחק',
              reset: 'איפוס',
            }
          }
        }
      ]
      },
      dataLabels: {
        enabled: false
      },
      xaxis: {
        categories: this.xdata,
        min: new Date(dateShifted).getTime(),
        type: 'datetime',
        labels: {
          show: true,
          rotate: 0,
          rotateAlways: true,
          format: this.date_format,
        },
        crosshairs: {
          show: true,
          position: 'back',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          formatter: undefined,
          offsetY: 0,
      },
      },
      yaxis: {
        show: true,
        showAlways: true,
        tickAmount: 5,
        opposite: main_chart_settings.language.english, //axis to right: true/false
        floating: true,
        decimalsInFloat:0,
        min: y_min_year - y_scale_shift,
        max: y_max_year + y_scale_shift,
        labels: {
          show: true,
          style: {
              fontSize: '10px',
              fontFamily: 'Helvetica, Arial, sans-serif',
              fontWeight: 400,
              cssClass: 'apexcharts-yaxis-dice',
          },
          offsetX: 40,
          offsetY: -6,
          formatter: function (val) {
              return val.toFixed(0);
          }
      },
      crosshairs: {
        show: true,
        position: 'front',
        stroke: {
            color: '#b6b6b6',
            width: 1,
            dashArray: 2,
        },
      },
      tooltip: {
        enabled: true,
        offsetX: 0,
      },
      },
      tooltip: {
        x: {  //The tooltip header with date
          show: true,
          format: 'dd/MM/yyyy',
          formatter: undefined,
      },
        y: {
          formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
            if (main_chart_settings.language.hebrew){
              var info_box_txt = '<div align="right" class="arrow_box">' +
              '<span><b>' + main_chart_settings.info_box.version_hebrew.close_txt + series[seriesIndex][dataPointIndex] + '</b></span>' + '<br>' +
              '<span>' + main_chart_settings.info_box.version_hebrew.open_txt + five_year_opening_rate[dataPointIndex] + '</span>' + '<br>' +
              '<span>' + main_chart_settings.info_box.version_hebrew.high_txt + five_year_daily_high[dataPointIndex] + '</span>' + '<br>' +
              '<span>' + main_chart_settings.info_box.version_hebrew.low_txt +  five_year_daily_low[dataPointIndex] + '</span>' + '<br>' +
              '</div>';
            }
            else{
              var info_box_txt = '<div align="left" class="arrow_box">' +
              '<span><b>' + main_chart_settings.info_box.version_english.close_txt + series[seriesIndex][dataPointIndex] + '</b></span>' + '<br>' +
              '<span>' + main_chart_settings.info_box.version_english.open_txt + five_year_opening_rate[dataPointIndex] + '</span>' + '<br>' +
              '<span>' + main_chart_settings.info_box.version_english.high_txt + five_year_daily_high[dataPointIndex] + '</span>' + '<br>' +
              '<span>' + main_chart_settings.info_box.version_english.low_txt +  five_year_daily_low[dataPointIndex] + '</span>' + '<br>' +
              '</div>';
            }
            return info_box_txt
          },
          title: {
            formatter: (seriesName) => '',
        },
        },
        marker: {
          show: false,
        },
        fixed: {
          enabled: true,
          position: tool_box_position,
          offsetX: tool_box_offset_x,
          offsetY: -25,
      },

      },
      stroke: {
        show: true,
        curve: 'smooth',
        width: _chart_stroke,
        dashArray: 0,      
      },
      grid: {
        row: {
            colors: ['#ffffff', 'transparent'],
            opacity: 0.2
        }, 
        column: {
            colors: ['#f8f8f8', 'transparent'],
            opacity: 0.1
        }, 
        xaxis: {
          lines: {
            show: false
          }
        },
        padding: {
          left: 0,
          right: 0
        }
      },
        fill: {
          opacity: 0.3,
          gradient: {
              opacityFrom: 0.6,
              opacityTo: 0.2,
          },
        },
      colors: _chart_color,
      theme: {
        //Themes: https://apexcharts.com/docs/options/theme/
        mode: _chart_mode,  
      },
      };

    var chart = new ApexCharts(document.querySelector(this.htmlid), options);
    chart.render();
    _debugLogger("[!] chartSoldier(): Rendered chart");

    var resetCssClasses = function (activeEl) {
      var els = document.querySelectorAll("button");
      Array.prototype.forEach.call(els, function (el) {
        el.classList.remove('active');
      });
    
      activeEl.target.classList.add('active')
    }

    //1Y
    document.querySelector("#one_year").addEventListener('click', function (e) {
      resetCssClasses(e)
      var dateShifted = _generate_date_YEARS_back(new Date(), 1);
      var dateNow = _generate_date_YEARS_back(new Date(), 0);
      _debugLogger("[!] 1Y - back - Date: " + dateShifted);
      _debugLogger("[!] Date now: " + dateNow);
      chart.updateOptions({
        xaxis: {
          min: new Date(dateShifted).getTime(),
          max: new Date(dateNow).getTime(),
          labels: {
            show: true,
            rotate: 0,
            rotateAlways: true,
            format: 'MMM yy',
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
        },
        yaxis: {
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: y_min_year - y_scale_shift,
          max: y_max_year + y_scale_shift,
          labels: {
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        stroke: {
          width: _chart_stroke,
          curve: 'straight',
        }
      })
    })

    //3Y
    document.querySelector("#three_year").addEventListener('click', function (e) {
      resetCssClasses(e)
      var dateShifted = _generate_date_YEARS_back(new Date(), 3);
      var dateNow = _generate_date_YEARS_back(new Date(), 0);
      _debugLogger("[!] 3Y - back - Date: " + dateShifted);
      chart.updateOptions({
        xaxis: {
          min: new Date(dateShifted).getTime(),
          max: new Date(dateNow).getTime(),
          labels: {
            show: true,
            rotate: 0,
            rotateAlways: true,
            format: 'MMM yy',
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
        },
        yaxis: {
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: y_min_threeyear - y_scale_shift,
          max: y_max_threeyear + y_scale_shift,
          labels: {
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        stroke: {
        show: true,
        curve: 'smooth',
        width: _chart_stroke,
        dashArray: 0, 
        }
      })
    })

    //5Y
    document.querySelector("#five_year").addEventListener('click', function (e) {
      resetCssClasses(e)
      var dateShifted = _generate_date_YEARS_back(new Date(), 5);
      var dateNow = _generate_date_YEARS_back(new Date(), 0);
      _debugLogger("[!] 5Y - back - Date: " + dateShifted);
      chart.updateOptions({
        xaxis: {
          min: new Date(dateShifted).getTime(),
          max: new Date(dateNow).getTime(),
          labels: {
            offsetX: 40,
            offsetY: -6,
            show: true,
            rotate: 0,
            rotateAlways: true,
            format: 'MMM yy',
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
        },
        yaxis: {
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: y_min_fiveyear - y_scale_shift,
          max: y_max_fiveyear + y_scale_shift,
          labels: {
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }          
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        stroke: {
        show: true,
        curve: 'smooth',
        width: _chart_stroke,
        dashArray: 0, 
        }
      })
    })

    //3M
    document.querySelector("#three_month").addEventListener('click', function (e) {
      resetCssClasses(e)
      var dateShifted = _generate_date_MONTHS_back(new Date(), 3);
      var dateNow = _generate_date_YEARS_back(new Date(), 0);
      _debugLogger("[!] 3M - back - Date: " + dateShifted);
      _debugLogger("[!] 3M - max: " + y_max_threemonth);
      _debugLogger("[!] 3M - min: " + y_min_threemonth);
      chart.updateOptions({
        xaxis: {
          min: new Date(dateShifted).getTime(),
          max: new Date(dateNow).getTime(),
          labels: {
            offsetX: 40,
            offsetY: -6,
            show: true,
            rotate: 0,
            rotateAlways: true,
            format: 'dd/MM/yy',
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
        },
        yaxis: {
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: y_min_threemonth - y_scale_shift,
          max: y_max_threemonth + y_scale_shift,
          labels: {
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        stroke: {
        show: true,
        curve: 'smooth',
        width: _chart_stroke,
        dashArray: 0, 
        }
      })
    })

    //6M
    document.querySelector("#six_month").addEventListener('click', function (e) {
      resetCssClasses(e)
      var dateShifted = _generate_date_MONTHS_back(new Date(), 6);
      var dateNow = _generate_date_YEARS_back(new Date(), 0);
      _debugLogger("[!] 6M - back - Date: " + dateShifted);
      _debugLogger("[!] 6M - max : " + y_max_sixmonth);
      _debugLogger("[!] 6M - min : " + y_min_sixmonth);

      chart.updateOptions({
        xaxis: {
          min: new Date(dateShifted).getTime(),
          max: new Date(dateNow).getTime(),
          labels: {
            show: true,
            rotate: 0,
            rotateAlways: true,
            format: 'MMM yy',
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
        },
        yaxis: {
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: y_min_sixmonth - y_scale_shift,
          max: y_max_sixmonth + y_scale_shift,
          labels: {
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        stroke: {
        show: true,
        curve: 'smooth',
        width: _chart_stroke,
        dashArray: 0, 
        }
      })
    })

    //1M
    document.querySelector("#one_month").addEventListener('click', function (e) {
      resetCssClasses(e)
      var date_now_from_dataset = new Date(last_timestamp);
      var dateShifted = _generate_date_MONTHS_back(new Date(), 1);
      var dateNow = _generate_date_YEARS_back(new Date(date_now_from_dataset), 0);
      
      _debugLogger("[!] 1M - back - Date: " + dateShifted);
      chart.updateOptions({
        xaxis: {
          min: new Date(dateShifted).getTime(),
          max: new Date(dateNow).getTime(),
          labels: {
            show: true,
            rotate: 0,
            rotateAlways: true,
            format: 'dd/MM/yy',
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
        },
        yaxis: {
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: y_min_onemonth - y_scale_shift,
          max: y_max_onemonth + y_scale_shift,
          labels: {
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        stroke: {
        show: true,
        curve: 'smooth',
        width: _chart_stroke,
        dashArray: 0, 
        }
      })
    })

  //END
  }

}