  // TODO add config.js file and there write all constants.
 var env = 'wordpress'; //log code debug in console | options: dev (Development mode) / prod (not used ?) / wordpress (production)

 var _api_domain = 'https://finance.irfeed.co.il';
 var _api_key = 'e3e39a6ba77ad26ac31b';
 var _api_endpoint = '/api/getHistoricalData';
 var _api_endpoint_interested_parties = '/api/getInterestedParties';


// * Interested parties settings objects * //

const int_parties_chart_settings = {
  language: {
    english: false,
    hebrew: true
  }
}

const int_parties_table_settings = {
  language: {
    english: false,
    hebrew: true
  },

  // table headers
  tableHeaders_heb: ["בעלי עניין", "%"],
  tableHeaders_eng: ["Shareholders", "%"],

  //Interested parties pie chart colors plate
  pie_color_01: '#193498',
  pie_color_02: '#113CFC',
  pie_color_03: '#1597E5',
  pie_color_04: '#69DADB',
  pie_color_05: '#F44336',
  pie_color_06: '#E91E63',
  pie_color_07: '#9C27B0',
  pie_color_08: '#000D6B',
  pie_color_09: '#9C19E0',
  pie_color_10: '#FF5DA2',
  pie_color_11: '#99DDCC',
  pie_color_12: '#49FF00',
  pie_color_13: '#49FF00',
  pie_color_14: '#49FF00',
  pie_color_15: '#49FF00',
  pie_color_16: '#49FF00',
  pie_color_17: '#49FF00',
  pie_color_18: '#49FF00',
  pie_color_19: '#49FF00',
  pie_color_20: '#49FF00',
  pie_color_21: '#49FF00'
}

// * Chart setting objects * //
const main_chart_settings = {
  language: {
    english: false,
    hebrew: true
  },
  info_box: {
    version_english: {
      open_txt: "Open: ",
      close_txt: "Close: ",
      high_txt: "High: ",
      low_txt: "Low: ",
      change_txt: "Change: ",
      price_txt: "Price: ",
      offsetX: 10,
      position: "bottomLeft"
    },
    version_hebrew: {
      open_txt: "פתיחה: ",
      close_txt: "סגירה: ",
      high_txt: "ערך גבוה: ",
      low_txt: "ערך נמוך: ",
      change_txt: "אחוז שינוי: ",
      price_txt: "מחיר: ",
      offsetX: -10,
      position: "bottomRight"
    }
  }
}


// Global Alerts Texts
var _missing_data_text = '<div class="nodata-text">Data is missing. Service is under maintenance.</div>'

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
console.log('[!!!][URL Params] Query string: ' + queryString);  //if nothing: empty - checked in env conditions

var _api_stock_number = false;
//var _chart_mode = 'light';
//var _chart_type = 'area';
//var _chart_stroke = '1';
//var _chart_height = '350';
//var _chart_color = ['#000'];
//var _chart_colorbox = '#000';
//var _chart_markersize = '2';

//Control y axis top/bottom spaces
var _y_axis_shift = 2;

//Control mini chart Y annotation offset
var _miniChart_annotation_y_offset = 100;

//WordPress environment - run logs thrown to console
if (env == 'wordpress'){
    _api_key = api_object.apiKey;
    _api_stock_number = api_object.stockNumber;
    _chart_mode = api_object.mode;
    _chart_type = api_object.type;
    _chart_stroke = api_object.stroke;
    _chart_height = api_object.height;
    _chart_color = api_object.color ? api_object.color : _chart_color;
    _chart_colorbox = api_object.colorbox;
    _chart_markersize = api_object.markersize;
}

//Development environment - run logs thrown to console
 if (env == 'dev'){
  var _chart_color = ['#4287f5'];
  var _chart_colorbox = '#4287f5';
  //stock
   if(urlParams.get('stock')){
     _api_stock_number = urlParams.get('stock');
    console.log('[!!!][URL Params] Using URL Param stock: ' + _api_stock_number);
   }
   else{
     _api_stock_number = '475020';
    console.log('[!!!][URL Params] Using default stock: ' + _api_stock_number);
   }

  //mode
   if(urlParams.get('mode')){
     _chart_mode = urlParams.get('mode');
    console.log('[!!!][URL Params] Using URL Param mode: ' + _chart_mode);
   }
   else{
     _chart_mode = 'light';
    console.log('[!!!][URL Params] Using default mode: ' + _chart_mode);
   }

  //type
   if(urlParams.get('type')){
     _chart_type = urlParams.get('type');
    console.log('[!!!][URL Params] Using URL Param type: ' + _chart_type);
   }
   else{
     _chart_type = 'area';
    console.log('[!!!][URL Params] Using default type: ' + _chart_type);
   }

  //stroke
   if(urlParams.get('stroke')){
     _chart_stroke = urlParams.get('stroke');
    console.log('[!!!][URL Params] Using URL Param stroke: ' + _chart_stroke);
   }
   else{
     _chart_stroke = '1';
    console.log('[!!!][URL Params] Using default stroke: ' + _chart_stroke);
   }

  //height
   if(urlParams.get('height')){
     _chart_height = urlParams.get('height');
    console.log('[!!!][URL Params] Using URL Param height: ' + _chart_height);
   }
   else{
     _chart_height = 350;
    console.log('[!!!][URL Params] Using default height: ' + _chart_height);
   }
   
 }

 //Production environment - no logs to console
 else if (env == 'prod'){
  /*
    stock param: if wrong/missing - no default - will throw error in console.
  */

  //stock
  if(urlParams.get('stock')){
     _api_stock_number = urlParams.get('stock');
    console.log('[!!!][URL Params] Using URL Param stock: ' + _api_stock_number);
   }
   else{
    throw new Error('[!!!][URL Params] Failed using stock param');
   }

  //mode
  if(urlParams.get('mode')){
     _chart_mode = urlParams.get('mode');
    console.log('[!!!][URL Params] Using URL Param mode: ' + _chart_mode);
   }
  else{
    _chart_mode = 'light';
    throw new Error('[!!!][URL Params] Failed using mode param');
   }

  //type
  if(urlParams.get('type')){
     _chart_type = urlParams.get('type');
    console.log('[!!!][URL Params] Using URL Param type: ' + _chart_type);
   }
  else{
    _chart_type = 'area';
    throw new Error('[!!!][URL Params] Failed using type param');
   }

  //stroke
  if(urlParams.get('stroke')){
     _chart_stroke = urlParams.get('stroke');
    console.log('[!!!][URL Params] Using URL Param stroke: ' + _chart_stroke);
   }
  else{
    _chart_stroke = '1';
    throw new Error('[!!!][URL Params] Failed using stroke param');
   }

  //height
  if(urlParams.get('height')){
     _chart_height = urlParams.get('height');
    console.log('[!!!][URL Params] Using URL Param height: ' + _chart_height);
   }
  else{
    _chart_height = 350;
    throw new Error('[!!!][URL Params] Failed using height param');
   }
 }

//WordPress environment
else if (env == 'wordpress'){
  _api_key = api_object.apiKey;
  _api_stock_number = api_object.stockNumber;
  _chart_mode = api_object.mode;
  _chart_type = api_object.type;
  _chart_stroke = api_object.stroke;
  _chart_height = api_object.height;
  _chart_color = api_object.color ? api_object.color : _chart_color;
  _chart_colorbox = api_object.colorbox;
  _chart_markersize = api_object.markersize;
  _chart_y_axis_shift = api_object.y_axis_shift;

  int_parties_table_settings.pie_color_01 = api_object.piecolor01;
  int_parties_table_settings.pie_color_02 = api_object.piecolor02;
  int_parties_table_settings.pie_color_03 = api_object.piecolor03;
  int_parties_table_settings.pie_color_04 = api_object.piecolor04;
  int_parties_table_settings.pie_color_05 = api_object.piecolor05;
  int_parties_table_settings.pie_color_06 = api_object.piecolor06;
  int_parties_table_settings.pie_color_07 = api_object.piecolor07;
  int_parties_table_settings.pie_color_08 = api_object.piecolor08;
  int_parties_table_settings.pie_color_09 = api_object.piecolor09;
  int_parties_table_settings.pie_color_10 = api_object.piecolor10;
  int_parties_table_settings.pie_color_11 = api_object.piecolor11;
  int_parties_table_settings.pie_color_12 = api_object.piecolor12;
  int_parties_table_settings.pie_color_13 = api_object.piecolor13;
  int_parties_table_settings.pie_color_14 = api_object.piecolor14;
  int_parties_table_settings.pie_color_15 = api_object.piecolor15;
  int_parties_table_settings.pie_color_16 = api_object.piecolor16;
  int_parties_table_settings.pie_color_17 = api_object.piecolor17;
  int_parties_table_settings.pie_color_18 = api_object.piecolor18;
  int_parties_table_settings.pie_color_19 = api_object.piecolor19;
  int_parties_table_settings.pie_color_20 = api_object.piecolor20;
  int_parties_table_settings.pie_color_21 = api_object.piecolor21;

  int_parties_chart_settings.language.hebrew = api_object.parties_chart_hebrew;
  int_parties_table_settings.language.hebrew = api_object.parties_table_hebrew;
  main_chart_settings.language.hebrew = api_object.hebrew;
  
}

function main(chartType) {
  /*
  pull data 1 req for all graphs data
  BuildChart class: pulls the whole data, navigates it to next step: data construction.
  chartTypes: 
    - mainChart
    - miniChart
    - miniChart2
    - pie_interested_parties
  */

 _debugLogger('[!] Starting App: Time: ' + Date(Date.now()).toString());

  stockCharts = new BuildChart(chartType);
}
