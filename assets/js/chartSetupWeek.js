class chartSetupWeek{
    constructor(y_data, 
                x_data,
                htmlid, 
                type, 
                date_format,
                day_x_axis_limit,
                day_x_axis_annotation,
                oneweek_x_tooltip,
                y_max_oneweek, 
                y_min_oneweek){
      /*
      y_data: the y data passed as array
      x_data: the x data passed as array
      htmlid: the ID where to implement the chart
      type: chart type: year/month... free text
      date_format: in which format show the dates
      max/min values: used for fitting Y axis dynamically
      */
  
      _debugLogger("[!] Creating chartSetup class for: " + type);
      this.ydata = y_data;
      this.xdata = x_data;
      this.htmlid = htmlid;
      this.type = type;
      this.date_format = date_format;
      this.x_axis_limit = day_x_axis_limit;
      this.day_x_axis_annotation = day_x_axis_annotation;
      this.y_max_oneweek = y_max_oneweek;
      this.y_min_oneweek = y_min_oneweek;

      _debugLogger("[!] Date format: " + this.date_format);
      _debugLogger("[!] Annotation - X axis: " + this.day_x_axis_annotation);
      _debugLogger("[!] Min - Y axis: " + this.y_min_oneweek);
      _debugLogger("[!] Max - Y axis: " + this.y_max_oneweek);
  
      /*
      Globals:
      */
     this.y_scale_shift = _y_axis_shift; //Scale the dynamic y borders - down/up
  
      //Deploy chart
      this.chartSoldier(oneweek_x_tooltip);
    }
  
    chartSoldier(oneweek_x_tooltip){
      /*
        Build the chart for this object
      */

      _debugLogger("[!] into chartSoldier() - Constructing weekly chart.");

      var info_box_txt_week = '';
      if (main_chart_settings.language.hebrew){
        var tool_box_offset_x_week = main_chart_settings.info_box.version_hebrew.offsetX;
        var tool_box_position_week = main_chart_settings.info_box.version_hebrew.position;
      }
      else{
        var tool_box_offset_x_week = main_chart_settings.info_box.version_english.offsetX;
        var tool_box_position_week = main_chart_settings.info_box.version_english.position;
      }

      var options = {
        series: [{
          data: this.ydata,
        },
        ],
        chart: {
          id: 'chart2',
          type: _chart_type,
          width:'100%',
          height: _chart_height,
          toolbar: {
            tools: {
              zoom: false,
              zoomin: '<button class="btn-ideadice-toolbar"><i class="fas fa-plus"></i></i></button>',
              zoomout: '<button class="btn-ideadice-toolbar"><i class="fas fa-minus"></i></button>',
              pan: '<button class="btn-ideadice-toolbar"><i class="fas fa-ellipsis-h"></i></button>',
              download: '<button class="btn-ideadice-toolbar"><i class="fas fa-cloud-download-alt"></i></button>',
              reset: false,
            },
          },
          offsetX: 0,
        },
        grid: {
        row: {
            colors: ['#ffffff', 'transparent'],
            position: 'back',
            opacity: 0.2
        }, 
        column: {
            colors: ['#f8f8f8', 'transparent'],
            opacity: 0.1
        }, 
        xaxis: {
          lines: {
            show: false
          }
        },
        padding: {
          left: 0,
          right: 0
        }
        },
        dataLabels: {
          enabled: false
        },
        xaxis: {
          categories: this.xdata,
          //max: new Date(this.x_axis_limit).getTime(),
          //type: 'datetime',
          type: 'category',
          tickAmount: 10,
          labels: {
            show: true,
            rotate: 0,
            rotateAlways: false,
            hideOverlappingLabels: true,
            showDuplicates: false,
            trim: false,
          },
          crosshairs: {
            show: true,
            position: 'back',
            stroke: {
                color: '#b6b6b6',
                width: 1,
                dashArray: 2,
            },
          },
          tooltip: {
            enabled: true,
            formatter: undefined,
            offsetY: 0,
        },
        },
        yaxis: {
          show: true,
          showAlways: true,
          tickAmount: 5,
          opposite: true, //axis to right
          floating: true,
          decimalsInFloat:0,
          min: this.y_min_oneweek - this.y_scale_shift,
          max: this.y_max_oneweek + this.y_scale_shift,
          labels: {
            show: true,
            style: {
                fontSize: '10px',
                fontFamily: 'Helvetica, Arial, sans-serif',
                fontWeight: 400,
                cssClass: 'apexcharts-yaxis-dice',
            },
            offsetX: 40,
            offsetY: -6,
            formatter: function (val) {
                return val.toFixed(0);
            }
        },
        crosshairs: {
          show: true,
          position: 'front',
          stroke: {
              color: '#b6b6b6',
              width: 1,
              dashArray: 2,
          },
        },
        tooltip: {
          enabled: true,
          offsetX: 0,
        },
        },
        markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
        tooltip: {
          x: {
            //show: true,
            //format: 'dd/MM/yyyy HH:mm',
            //formatter: undefined,
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
              return oneweek_x_tooltip[dataPointIndex];
            },
        },
        y: {
          formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
            if (main_chart_settings.language.hebrew){
              info_box_txt_week = '<div align="right" class="arrow_box">' +
              '<span>' + main_chart_settings.info_box.version_hebrew.price_txt + series[seriesIndex][dataPointIndex] + '</span>' + '<br>' +
              //'<span>' + 'Rate - Prev: ' + series[seriesIndex][dataPointIndex-1] + '</span>' + '<br>' +
              _change_rate_percent_html_tag(series[seriesIndex][dataPointIndex], series[seriesIndex][dataPointIndex-1]) +
              '</div>';
            }
            else{
              info_box_txt_week = '<div align="left" class="arrow_box">' +
              '<span>' + main_chart_settings.info_box.version_english.price_txt + series[seriesIndex][dataPointIndex] + '</span>' + '<br>' +
              //'<span>' + 'Rate - Prev: ' + series[seriesIndex][dataPointIndex-1] + '</span>' + '<br>' +
              _change_rate_percent_html_tag(series[seriesIndex][dataPointIndex], series[seriesIndex][dataPointIndex-1]) +
              '</div>';
            }

            return info_box_txt_week;
          },
          title: {
            formatter: (seriesName) => '',
        },
        },
          marker: {
            show: false,
          },
          fixed: {
            enabled: true,
            position: tool_box_position_week,
            offsetX: tool_box_offset_x_week,
            offsetY: -25,
        },
        },
        stroke: {
          show: true,
          curve: 'straight',
          width: _chart_stroke,
          dashArray: 0,      
        },
        fill: {
          opacity: 0.3,
          gradient: {
              opacityFrom: 0.6,
              opacityTo: 0.2,
          },
        },
      colors: _chart_color,
      theme: {
        //Themes: https://apexcharts.com/docs/options/theme/
        mode: _chart_mode,  
      },
        };
  
      var chart = new ApexCharts(document.querySelector(this.htmlid), options);
      chart.render();
      _debugLogger("[!] chartSoldierWeek(): Rendered chart");
  
      var resetCssClasses = function (activeEl) {
        var els = document.querySelectorAll("button");
        Array.prototype.forEach.call(els, function (el) {
          el.classList.remove('active');
        });
      
        activeEl.target.classList.add('active')
      }
    }
  }