class miniChart{
    constructor(y_data, 
                x_data,
                y_max_minichart,
                y_min_minichart,
                miniChart_realtime_annotation,
                day_x_axis_limit,
                htmlid,
                type){
      /*
      y_data: the y data passed as array
      x_data: the x data passed as array
      type: chart type: year/month... free text
      */
  
      _debugLogger("[!] Creating chart class for: " + type);
      this.ydata = y_data;
      this.xdata = x_data;
      this.y_max_minichart = y_max_minichart;
      this.y_min_minichart = y_min_minichart;
      this.day_x_axis_annotation = miniChart_realtime_annotation;
      this.x_axis_limit = day_x_axis_limit;
      this.y_scale_shift = _y_axis_shift;
      this.htmlid = htmlid;

      _debugLogger("[!] Annotation - X axis: " + this.day_x_axis_annotation);

      /*
      Globals
      */
     this.height = _chart_height;
     this.width = '100%';
      //Deploy chart
      this.chartSoldier();
    }
  
    chartSoldier(){
      /*
        Build the chart for this object
      */

      _debugLogger("[!] into chartSoldier() - Constructing mini chart.");

      var options = {
        series: [{
        data: this.ydata
        }],
        xaxis: {
        categories: this.xdata,
        type: 'datetime',
        max: new Date(this.x_axis_limit).getTime(),
        },
        chart: {
        type: _chart_type,
        height: this.height,
        width: this.width,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        width: _chart_stroke,
        curve: 'straight'
      },
      markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
      tooltip: {
        x: {
          show: true,
          format: 'dd/MM/yyyy HH:mm',
          formatter: undefined,
        },
        y: {
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
              return '<div class="arrow_box">' +
              '<span>' + 'Price: ' + series[seriesIndex][dataPointIndex] + '</span>' + '<br>' +
              '</div>'
            },
            title: {
              formatter: (seriesName) => '',
          },
          },
        marker: {
        show: false,
        },
    },
      yaxis: {
        min: this.y_min_minichart - this.y_scale_shift,
        max: this.y_max_minichart + this.y_scale_shift,
      },
       fill: {
          opacity: 0.3,
          gradient: {
              opacityFrom: 0.6,
              opacityTo: 0.2,
          },
        },
      colors: _chart_color,
      theme: {
        //Themes: https://apexcharts.com/docs/options/theme/
        mode: _chart_mode,  
      },
      annotations: {
        xaxis: [
          {
            x: new Date(this.day_x_axis_annotation).getTime(),
            label: {
              borderColor: _chart_colorbox,
              orientation: 'horizontal',
              offsetY: _miniChart_annotation_y_offset,
              style: {
                color: _chart_colorbox,
              },
              text: 'Last Price'
            },
            borderColor: _chart_colorbox,
          }
        ]
      },
      //colors: ['#27496d'],
      };

      var chart = new ApexCharts(document.querySelector(this.htmlid), options);
      chart.render();

    }
  }

class miniChart2{
    constructor(y_data, 
                x_data,
                y_max_minichart,
                y_min_minichart,
                htmlid,
                type){
      /*
      y_data: the y data passed as array
      x_data: the x data passed as array
      type: chart type: year/month... free text
      */
  
      _debugLogger("[!] Creating chart class for: " + type);
      this.ydata = y_data;
      this.xdata = x_data;
      this.y_max_minichart = y_max_minichart;
      this.y_min_minichart = y_min_minichart;
      this.y_scale_shift = 0.3;
      this.htmlid = htmlid;

      /*
      Globals
      */
     this.height = _chart_height;
     this.width = '100%';
      //Deploy chart
      this.chartSoldier();
    }
  
    chartSoldier(){
      /*
        Build the chart for this object
      */

      _debugLogger("[!] into chartSoldier() - Constructing mini chart.");

      var options = {
        series: [{
        data: this.ydata
        }],
        xaxis: {
        categories: this.xdata,
        type: 'datetime',
        },
        chart: {
        type: _chart_type,
        height: this.height,
        width: this.width,
        sparkline: {
          enabled: true
        },
      },
      stroke: {
        width: _chart_stroke,
        curve: 'straight'
      },
      markers: {
          size: 0,
          hover: {
            size: _chart_markersize,
            sizeOffset: _chart_markersize
          },
        },
      tooltip: {
        x: {
          show: true,
          format: 'dd/MM/yyyy',
          formatter: undefined,
        },
        y: {
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
              return '<div class="arrow_box">' +
              '<span>' + 'Price: ' + series[seriesIndex][dataPointIndex] + '</span>' + '<br>' +
              '</div>'
            },
            title: {
              formatter: (seriesName) => '',
          },
          },
        marker: {
        show: false,
        },
    },
      yaxis: {
        min: this.y_min_minichart - this.y_scale_shift,
        max: this.y_max_minichart + this.y_scale_shift,
      },
       fill: {
          opacity: 0.3,
          gradient: {
              opacityFrom: 0.6,
              opacityTo: 0.2,
          },
        },
      colors: _chart_color,
      theme: {
        //Themes: https://apexcharts.com/docs/options/theme/
        mode: _chart_mode,  
      },

      //colors: ['#27496d'],
      };

      var chart = new ApexCharts(document.querySelector(this.htmlid), options);
      chart.render();

    }
  }