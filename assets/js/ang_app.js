var app = angular.module("chartsApp", []);

app.controller('mainController', function ($scope) {
    $scope.showFiveYear = true;
    $scope.hideDay = true;
    $scope.hideWeek = true;
    
    

    $scope.showHideFunc = function (param) {

        if (param == "showFiveYear") {
            $scope.showFiveYear = true;
            $scope.hideDay = true;
            $scope.hideWeek = true;
        }
        else if (param == "showDay") {
            $scope.showFiveYear = false;
            $scope.hideDay = false;
            $scope.hideWeek = true;
        }
        else if (param == "showWeek") {
            $scope.showFiveYear = false;
            $scope.hideDay = true;
            $scope.hideWeek = false;
        }
        else {
            $scope.showFiveYear = true;
            $scope.hideDay = true;
            $scope.hideWeek = true;
        }

    //END showHideFunc
    }




});