<?php


class StockQuote {

    public $data;

    public  function initial(){

        $api_key = get_option("api_key");
        $companies_number = get_option("companies_number");
        $stock_number = explode(",", $companies_number);
        $http_query = http_build_query(array('stock_number' => $stock_number), '', '&');
        $url = "https://finance.irfeed.co.il/api/getStocks?{$http_query}&api_key={$api_key}";
        $response = wp_remote_post( $url, array(
                'method' => 'GET',
                'timeout' => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => array(),
                'body' => array( ),
                'cookies' => array()
            )
        );

        if ( is_wp_error( $response ) ) {
            $error_message = $response->get_error_message();
            return "NO DATA"; // TODO
        } else {
            $data = json_decode($response['body']);
            if(isset($data->error_message) || isset($data->error_type) || isset($data->error)){
                $this->data = "ERROR";
            }

            $keys_arr = [
                'Rate',
                'BaseRate',
                'BaseRateChangePercentage',
                'BaseRateChange',
                'DailyHighestRate',
                'DailyLowestRate',
                'HebName',
                'EngName',
                'HebSymbol',
                'EngSymbol',
                'Exchange',
                'Symbol',
                'AllYearMaximumRate',
                'AllYearMinimumRate',
                'LastDealDate',
                'LastDealRate',
                'GrossYieldToMaturity',
                'GrossDuration',
                'ListedFortune',
                'LastKnownRate',
                'LastKnownRateDate',
                'LastKnownRateTime',
                'DailyTurnover',
                'Duality',
                'CompanyNum',
                'CorpNum',
                'CompanyMarketValue',
                'MarketWorth',
                'CurrencyCode',
                'TradingStage',
                'AllMonthMinimumRate',
                'AllMonthMaximumRate',
                'AllMonthAverageNIS',
                'QuarterMinimumRate',
                'QuarterMaximumRate',
                'QuarterAverageNIS',
                'UpToOneYearMinimumRate',
                'UpToOneYearMaximumRate',
                'UpToOneYearAverageNIS',
                'AllYearAverageNIS',
                'DailyNISRevenue'
            ];
            $arr_obj = [];
            $key = '-Key';
            foreach ($data as $obj){
                $obj_temp = new stdClass();
                foreach ($keys_arr as $field){
                    $obj_temp->{$field} = $this->initialValue($obj->{$field});
                }
                $arr_obj[$obj->stock_number] = $obj_temp;
            }
            $this->data = $arr_obj;
        }
    }

    public function initialValue($value){
        $temp_value = $this->data == 'ERROR' || empty($value) ? '0' : $value;
        return $temp_value;
    }

    public function initialClassname($val){
        $class_name = '';
        if($val != '0' && $val > 0){
            $class_name = "class=dice-data-up";
        }elseif($val != '0' && $val < 0){
            $class_name = "class=dice-data-down";
        }
        return $class_name;
    }

    public function __construct()
    {
        $this->initial();
    }

    public function addShortCode($short_code_name, $function_name){
        add_shortcode( $short_code_name, array($this, $function_name) );
    }
/* 001*/
    function get_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->Rate}</label>";
    }
/* 002*/
    function get_base_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->BaseRate}</label>";
    }
/* 003*/
    function get_base_rate_change_percentage($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);

        $class_name = $this->initialClassname($this->data[$atts['stock']]->BaseRateChange);
        return "<label dir='ltr'".$class_name.">{$this->data[$atts['stock']]->BaseRateChangePercentage}%</label>";
    }
/* 004*/
    function get_base_rate_change($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);

        $class_name = $this->initialClassname($this->data[$atts['stock']]->BaseRateChange);
        return "<label dir='ltr' ".$class_name.">{$this->data[$atts['stock']]->BaseRateChange}</label>";
    }
/* 005*/
    function get_daily_highest_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->DailyHighestRate}</label>";
    }
/* 006*/
    function get_daily_lowest_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->DailyLowestRate}</label>";
    }
/* 007*/
    function get_heb_name($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label>{$this->data[$atts['stock']]->HebName}</label>";
    }
/* 008*/
    function get_eng_name($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->EngName}</label>";
    }
/* 009*/
    function get_heb_symbol($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label>{$this->data[$atts['stock']]->HebSymbol}</label>";
    }
/* 010*/
    function get_eng_symbol($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->EngSymbol}</label>";
    }
/* 011*/
    function get_exchange($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->Exchange}</label>";
    }
/* 012*/
    function get_symbol($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->Symbol}</label>";
    }
/* 013*/
    function get_all_year_maximum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->AllYearMaximumRate}</label>";
    }
/* 014*/
    function get_all_year_minimum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->AllYearMinimumRate}</label>";
    }
/* 015*/
    function get_last_deal_date($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->LastDealDate}</label>";
    }
/* 016*/
    function get_last_deal_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->LastDealRate}</label>";
    }
/* 017*/
    function get_gross_yield_to_maturity($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->GrossYieldToMaturity}</label>";
    }
/* 018*/
    function get_gross_duration($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->GrossDuration}</label>";
    }
/* 019*/
    function get_listed_fortune($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->ListedFortune}</label>";
    }
/* 020*/
    function get_last_known_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->LastKnownRate}</label>";
    }
/* 021*/
    function get_last_Known_Rate_date($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->LastKnownRateDate}</label>";
    }
/* 022*/
    function get_last_Known_Rate_time($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        $last_known_rate_time = explode(":", $this->data[$atts['stock']]->LastKnownRateTime);
        $last_known_rate_time = $last_known_rate_time[0]. ":" . $last_known_rate_time[1];
        return "<label>{$last_known_rate_time}</label>";
    }
/* 023*/
    function get_daily_turnover($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->DailyTurnover}</label>";
    }
    
/* 024*/
    function get_duality($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->Duality}</label>";
    }
/* 025*/
    function get_company_num($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->CompanyNum}</label>";
    }
/* 026*/
    function get_corp_num($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->CorpNum}</label>";
    }
/* 027*/
    function get_company_market_value($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->CompanyMarketValue}</label>";
    }
/* 028*/
    function get_market_worth($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->MarketWorth}</label>";
    }
/* 029*/
    function get_currency_code($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->CurrencyCode}</label>";
    }
/* 030*/
    function get_trading_stage($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->TradingStage}</label>";
    }
/* 031*/
    function get_all_month_minimum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->AllMonthMinimumRate}</label>";
    }
/* 032*/
    function get_all_month_maximum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->AllMonthMaximumRate}</label>";
    }
/* 033*/
    function get_all_month_average_nis($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->AllMonthAverageNIS}</label>";
    }
/* 034*/
    function get_quarter_minimum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->QuarterMinimumRate}</label>";
    }
/* 035*/
    function get_quarter_maximum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->QuarterMaximumRate}</label>";
    }
/* 036*/
    function get_quarter_average_nis($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->QuarterAverageNIS}</label>";
    }
/* 037*/
    function get_up_to_one_year_minimum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->UpToOneYearMinimumRate}</label>";
    }
/* 038*/
    function get_up_to_one_year_maximum_rate($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->UpToOneYearMaximumRate}</label>";
    }
/* 039*/
    function get_up_to_one_year_average_nis($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->UpToOneYearAverageNIS}</label>";
    }
/* 040*/
    function get_all_year_average_nis($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->AllYearAverageNIS}</label>";
    }
/* 041*/
    function get_daily_nis_revenue($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<label dir='ltr'>{$this->data[$atts['stock']]->DailyNISRevenue}</label>";
    }
}
/* Create object of StockQuote class for create short codes of any value*/
$stock_obj = new StockQuote();
/* 001*/
$stock_obj->addShortCode('rate', 'get_rate');
/* 002*/
$stock_obj->addShortCode('base_rate', 'get_base_rate');
/* 003*/
$stock_obj->addShortCode('base_rate_change_percentage', 'get_base_rate_change_percentage');
/* 004*/
$stock_obj->addShortCode('base_rate_change', 'get_base_rate_change');
/* 005*/
$stock_obj->addShortCode('daily_highest_rate', 'get_daily_highest_rate');
/* 006*/
$stock_obj->addShortCode('daily_lowest_rate', 'get_daily_lowest_rate');
/* 007*/
$stock_obj->addShortCode('heb_name', 'get_heb_name');
/* 008*/
$stock_obj->addShortCode('eng_name', 'get_eng_name');
/* 009*/
$stock_obj->addShortCode('heb_symbol', 'get_heb_symbol');
/* 010*/
$stock_obj->addShortCode('eng_symbol', 'get_eng_symbol');
/* 011*/
$stock_obj->addShortCode('exchange', 'get_exchange');
/* 012*/
$stock_obj->addShortCode('symbol', 'get_symbol');
/* 013*/
$stock_obj->addShortCode('all_year_maximum_rate', 'get_all_year_maximum_rate');
/* 014*/
$stock_obj->addShortCode('all_year_minimum_rate', 'get_all_year_minimum_rate');
/* 015*/
$stock_obj->addShortCode('last_deal_date', 'get_last_deal_date');
/* 016*/
$stock_obj->addShortCode('last_deal_rate', 'get_last_deal_rate');
/* 017*/
$stock_obj->addShortCode('gross_yield_to_maturity', 'get_gross_yield_to_maturity');
/* 018*/
$stock_obj->addShortCode('gross_duration', 'get_gross_duration');
/* 019*/
$stock_obj->addShortCode('listed_fortune', 'get_listed_fortune');
/* 020*/
$stock_obj->addShortCode('last_known_rate', 'get_last_known_rate');
/* 021*/
$stock_obj->addShortCode('last_Known_Rate_date', 'get_last_Known_Rate_date');
/* 022*/
$stock_obj->addShortCode('last_Known_Rate_time', 'get_last_Known_Rate_time');
/* 023*/
$stock_obj->addShortCode('daily_turnover', 'get_daily_turnover');

/* 024*/
$stock_obj->addShortCode('duality', 'get_duality');
/* 025*/
$stock_obj->addShortCode('company_num', 'get_company_num');
/* 026*/
$stock_obj->addShortCode('corp_num', 'get_corp_num');
/* 027*/
$stock_obj->addShortCode('company_market_value', 'get_company_market_value');
/* 028*/
$stock_obj->addShortCode('market_worth', 'get_market_worth');
/* 029*/
$stock_obj->addShortCode('currency_code', 'get_currency_code');
/* 030*/
$stock_obj->addShortCode('trading_stage', 'get_trading_stage');
/* 031*/
$stock_obj->addShortCode('all_month_minimum_rate', 'get_all_month_minimum_rate');
/* 032*/
$stock_obj->addShortCode('all_month_maximum_rate', 'get_all_month_maximum_rate');
/* 033*/
$stock_obj->addShortCode('all_month_average_nis', 'get_all_month_average_nis');
/* 034*/
$stock_obj->addShortCode('quarter_minimum_rate', 'get_quarter_minimum_rate');
/* 035*/
$stock_obj->addShortCode('quarter_maximum_rate', 'get_quarter_maximum_rate');
/* 036*/
$stock_obj->addShortCode('quarter_average_nis', 'get_quarter_average_nis');
/* 037*/
$stock_obj->addShortCode('up_to_one_year_minimum_rate', 'get_up_to_one_year_minimum_rate');
/* 038*/
$stock_obj->addShortCode('up_to_one_year_maximum_rate', 'get_up_to_one_year_maximum_rate');
/* 039*/
$stock_obj->addShortCode('up_to_one_year_average_nis', 'get_up_to_one_year_average_nis');
/* 040*/
$stock_obj->addShortCode('all_year_average_nis', 'get_all_year_average_nis');
/* 041*/
$stock_obj->addShortCode('daily_nis_revenue', 'get_daily_nis_revenue');