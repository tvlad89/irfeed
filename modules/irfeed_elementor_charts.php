<?php
namespace Elementor;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;



class Irfeed_Charts extends Widget_Base {

	protected function generateRandomString($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function get_name() {
		return 'irfeed_elementor_charts';
	}
	
	public function get_title() {
		return __( 'Charts', 'irfeed' );
	}
	
	public function get_icon() {
		return 'eicon-container';
	}
	
	public function get_categories() {
		return [ 'irfeed-category' ];
	}
	
	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Chart Settings', 'irfeed' ),
			]
		);

		$this->add_control(
			'stock',
			[
				'label' => __( 'Stock Number', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Number', 'irfeed' ),
			]
		);

		$this->add_control(
			'key',
			[
				'label' => __( 'Key', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Key', 'irfeed' ),
			]
		);

		$this->add_control(
			'lang',
			[
				'label' => __( 'Hebrew RTL Chart', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'irfeed' ),
				'label_off' => __( 'No', 'irfeed' ),
				'return_value' => true,
				'default' => false,
			]
		);
		$this->add_control(
			'mode',
			[
				'label' => __( 'Mode', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'light',
				'options' => [
					'light'  => __( 'Light', 'irfeed' ),
					'dark'  => __( 'Dark', 'irfeed' ),
				],
				'separator' => 'before',
			]
		);
		$this->add_control(
			'type',
			[
				'label' => __( 'Type', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'area',
				'options' => [
					'area'  => __( 'Area', 'irfeed' ),
					'line'  => __( 'Line', 'irfeed' ),
				],
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'head_tabs_text',
			[
				'label' => esc_html__( 'Title', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Time', 'irfeed' ),
				'default' => esc_html__( 'Time', 'elementor' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);
		
		$repeater->add_control(
			'class_buttons_tabs',
			[
				'label' => __( 'class', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => 'class-none',
			]
		);

		$repeater->add_control(
			'id_buttons_tabs',
			[
				'label' => __( 'id', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => 'id-none',
			]
		);

		$repeater->add_control(
			'ng-click_buttons_tabs',
			[
				'label' => __( 'ng-click', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => 'ng-click-none',
			]
		);

		$repeater->add_control(
			'icon_buttons_tabs',
			[
				'label' => esc_html__( 'Icon', 'elementor' ),
				'type' => Controls_Manager::ICONS,
				'default' => [
					'value' => '',
					'library' => 'fa-solid',
				],
				'fa4compatibility' => 'icon',
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Chart Tabs List', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'separator' => 'before',
				'item_actions' => [
					'add'       => false,
					'duplicate' => false,
					'remove'    => false,
					'sort'      => false,
				],
				'default' => [
					[
						'head_tabs_text' => esc_html__( '1D', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'one_day',
						'ng-click_buttons_tabs' => 'showDay',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '1W', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'one_week',
						'ng-click_buttons_tabs' => 'showWeek',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '1M', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'one_month',
						'ng-click_buttons_tabs' => 'showFiveYear',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '3M', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'three_month',
						'ng-click_buttons_tabs' => 'showFiveYear',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '6M', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'six_month',
						'ng-click_buttons_tabs' => 'showFiveYear',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '1Y', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn active',
						'id_buttons_tabs' => 'one_year',
						'ng-click_buttons_tabs' => 'showFiveYear',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '3Y', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'three_year',
						'ng-click_buttons_tabs' => 'showFiveYear',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
					[
						'head_tabs_text' => esc_html__( '5Y', 'irfeed' ),
						'class_buttons_tabs' => 'irfeed-btn',
						'id_buttons_tabs' => 'five_year',
						'ng-click_buttons_tabs' => 'showFiveYear',
						'icon_buttons_tabs' => [
							'value' => '',
							'library' => 'fa-solid',
						],
					],
				],
				'title_field' => '{{{ elementor.helpers.renderIcon( this, icon_buttons_tabs, {}, "i", "panel" ) || \'<i class="{{ icon }}" aria-hidden="true"></i>\' }}} {{{ head_tabs_text }}}',
			]
		);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Chart Style', 'irfeed' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'content_typography',
				'label' => __( 'Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .irfeed-chart',
			]
		);

		$this->add_control(
			'height',
			[
				'label' => __( 'Height', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 5,
				'max' => 700,
				'step' => 1,
				'default' => 350,
			]
		);

		$this->add_control(
			'chart_color',
			[
				'label' => esc_html__( 'Chart Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);

		$this->add_control(
			'chart_colorbox',
			[
				'label' => esc_html__( 'Chart Price Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000000',

			]
		);

		$this->add_control(
			'stroke',
			[
				'label' => __( 'Line thickness', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 10,
				'step' => 1,
				'default' => 2,
			]
		);

		$this->add_control(
			'markersize',
			[
				'label' => __( 'Marker size', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 20,
				'step' => 1,
				'default' => 4,
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_tooltips',
			[
				'label' => __( 'Tooltips Style', 'irfeed' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		
		$this->add_control(
			'tooltip_box_title_color',
			[
				'label' => esc_html__( 'Tooltip Box Title Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .apexcharts-tooltip-title' => 'color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);
		$this->add_control(
			'tooltip_box_title_background_color',
			[
				'label' => esc_html__( 'Tooltip Box Title Background Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .apexcharts-tooltip.apexcharts-theme-light .apexcharts-tooltip-title' => 'background-color: {{WRAPPER}} !important;',
				],
			]
		);
		$this->add_control(
			'tooltip_box_title_typography',
			[
				'label' => __( 'Tooltip Data Box Title Text Size (px)', 'irfeed' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 8,
						'max' => 26,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 12,
				],
				'selectors' => [
					'{{WRAPPER}} .apexcharts-tooltip-title' => 'font-size: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);	

		$this->add_control(
			'tooltip_box_text_color',
			[
				'label' => esc_html__( 'Tooltip Box Text Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .arrow_box' => 'color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'tooltip_box_border_color',
			[
				'label' => esc_html__( 'Tooltip Box Border Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .apexcharts-tooltip.apexcharts-theme-light' => 'border: 1px solid {{WRAPPER}} !important;',
				],
			]
		);
		$this->add_control(
			'tooltip_box_background_color',
			[
				'label' => esc_html__( 'Tooltip Box Background Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .apexcharts-tooltip.apexcharts-theme-light' => 'background-color: {{WRAPPER}} !important;',
				],
			]
		);
		$this->add_control(
			'tooltip_box_typography',
			[
				'label' => __( 'Tooltip Data Box Text Size (px)', 'irfeed' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 8,
						'max' => 26,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 12,
				],
				'selectors' => [
					'{{WRAPPER}} .arrow_box' => 'font-size: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);	
		$this->add_control(
			'tooltip_axis_text_and_borders_color',
			[
				'label' => esc_html__( 'Axis Date Tooltip Text Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .apexcharts-xaxistooltip-text' => 'color: {{WRAPPER}} !important;',
					'{{WRAPPER}} .apexcharts-xaxistooltip' => 'border: 1px solid {{WRAPPER}} !important;',
					'{{WRAPPER}} .apexcharts-xaxistooltip-bottom:before' => 'border-bottom-color: {{WRAPPER}} !important;',
					'{{WRAPPER}} .apexcharts-yaxistooltip-text' => 'color: {{WRAPPER}} !important;',
					'{{WRAPPER}} .apexcharts-yaxistooltip' => 'border: 1px solid {{WRAPPER}} !important;',
					'{{WRAPPER}} .apexcharts-yaxistooltip-right:before' => 'border-right-color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);
		$this->add_control(
			'tooltip_axis_background_color',
			[
				'label' => esc_html__( 'Axis Date Tooltip Background Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .apexcharts-xaxistooltip' => 'background-color: {{WRAPPER}} !important;',
					'{{WRAPPER}} .apexcharts-yaxistooltip' => 'background-color: {{WRAPPER}} !important;',
				],
			]
		);

		$this->add_control(
			'tooltip_xaxis_typography',
			[
				'label' => __( 'X axis Date Tooltip Text Size (px)', 'irfeed' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 8,
						'max' => 26,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 12,
				],
				'selectors' => [
					'{{WRAPPER}} .apexcharts-xaxistooltip-text' => 'font-size: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .apexcharts-yaxistooltip-text' => 'font-size: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);
		$this->end_controls_section();
		$this->start_controls_section(
			'tabs_style',
			[
				'label' => __( 'Tabs Style', 'irfeed' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->start_controls_tabs( 'tabs_button_style' );
		$this->start_controls_tab(
			'button_tab_normal',
			[
				'label' => esc_html__( 'Normal', 'elementor' ),
			]
		);
		$this->add_control(
			'tabs_align',
			[
				'label' => __( 'Alignment', 'elementor' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'start' => [
						'title' => __( 'Left', 'irfeed' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'irfeed' ),
						'icon' => 'fa fa-align-center',
					],
					'end' => [
						'title' => __( 'Right', 'irfeed' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justify', 'irfeed' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => 'center',
				'toggle' => true,
				'prefix_class' => 'mainbuttons-layout-',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'buttons_typography',
				'label' => __( 'Button Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .irfeed-btn',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'button_background_color',
				'label' => __( 'Button Background Color', 'irfeed' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .irfeed-btn',
			]
		);
		$this->add_control(
			'button_color',
			[
				'label' => esc_html__( 'Button Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222',
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn' => 'color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border',
				'label' => __( 'Buttons Border', 'irfeed' ),
				'selector' => '{{WRAPPER}} .irfeed-btn',
			]
		);
		$this->add_control(
			'button_border_radius',
			[
				'label' => esc_html__( 'Button Border Radius', 'irfeed' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'button_padding',
			[
				'label' => __( 'Button Padding', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}!important;',
				],
			]
		);
		$this->end_controls_tab();
		$this->start_controls_tab(
			'button_tab_hover',
			[
				'label' => esc_html__( 'Hover', 'elementor' ),
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'buttons_typography_hover',
				'label' => __( 'Button Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .irfeed-btn:hover',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'button_background_color_hover',
				'label' => __( 'Button Background Color', 'irfeed' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .irfeed-btn:hover',
			]
		);
		$this->add_control(
			'button_color_hover',
			[
				'label' => esc_html__( 'Button Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222',
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn:hover' => 'color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border_hover',
				'label' => __( 'Buttons Border', 'irfeed' ),
				'selector' => '{{WRAPPER}} .irfeed-btn:hover',
			]
		);
		$this->add_control(
			'button_border_radius_hover',
			[
				'label' => esc_html__( 'Button Border Radius', 'irfeed' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->start_controls_tab(
			'button_tab_focus',
			[
				'label' => esc_html__( 'Focus', 'elementor' ),
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'buttons_typography_focus',
				'label' => __( 'Button Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .irfeed-btn:focus, {{WRAPPER}} .active',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'button_background_color_focus',
				'label' => __( 'Button Background Color', 'irfeed' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .irfeed-btn:focus, {{WRAPPER}} .active',
			]
		);
		$this->add_control(
			'button_color_focus',
			[
				'label' => esc_html__( 'Button Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222',
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn:focus, {{WRAPPER}} .active' => 'color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border_focus',
				'label' => __( 'Buttons Border', 'irfeed' ),
				'selector' => '{{WRAPPER}} .irfeed-btn:focus, {{WRAPPER}} .active',
			]
		);
		$this->add_control(
			'button_border_radius_focus',
			[
				'label' => esc_html__( 'Button Border Radius', 'irfeed' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .irfeed-btn:focus, {{WRAPPER}} .active' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();
	    $this->end_controls_tabs();
		$this->end_controls_section();

		$this->start_controls_section(
			'toolbar_style',
			[
				'label' => __( 'Toolbar Style', 'irfeed' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->start_controls_tabs( 'tabs_toolbar_style' );
		$this->start_controls_tab(
			'toolbar_tab_normal',
			[
				'label' => esc_html__( 'Normal', 'elementor' ),
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'toolbar_typography',
				'label' => __( 'Toolbar Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .btn-ideadice-toolbar',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'toolbar_background_color',
				'label' => __( 'Toolbar Background Color', 'irfeed' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .btn-ideadice-toolbar',
			]
		);
		$this->add_control(
			'toolbar_color',
			[
				'label' => esc_html__( 'Toolbar Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222',
				'selectors' => [
					'{{WRAPPER}} .btn-ideadice-toolbar' => 'color: {{WRAPPER}} !important;',
				],
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'toolbar_border',
				'label' => __( 'Toolbar Border', 'irfeed' ),
				'selector' => '{{WRAPPER}} .btn-ideadice-toolbar',
			]
		);
		$this->add_control(
			'toolbar_border_radius',
			[
				'label' => esc_html__( 'Toolbar Border Radius', 'irfeed' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .btn-ideadice-toolbar' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'toolbar_padding',
			[
				'label' => __( 'Toolbar Padding', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .btn-ideadice-toolbar' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}!important;',
				],
			]
		);
		$this->end_controls_tab();
		$this->start_controls_tab(
			'button_tab_hover',
			[
				'label' => esc_html__( 'Hover', 'elementor' ),
			]
		);

		$this->end_controls_tab();
		$this->start_controls_tab(
			'button_tab_focus',
			[
				'label' => esc_html__( 'Focus', 'elementor' ),
			]
		);

		$this->end_controls_tab();
	    $this->end_controls_tabs();
		$this->end_controls_section();


	}

	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );

	}

	protected function render( $data = [], $args = null ) {
			parent::render( $data, $args );
			$settings = $this->get_settings_for_display();
			$id_gen = $this->generateRandomString();
			wp_enqueue_script( 'irfeed-elementor-main', plugin_dir_url( __DIR__ ) . 'assets/js/main.js' );
			wp_enqueue_style( 'irfeed-elementor-charts', plugin_dir_url( __DIR__ ) . 'assets/css/charts.css' );
			
			wp_localize_script( 'irfeed-elementor-main', 'api_object',
				array(
					'apiKey' => $settings['key'],
					'stockNumber' => $settings['stock'],
					'mode' =>  $settings['mode'],
					'type' =>  $settings['type'],
					'stroke' =>  $settings['stroke'],
					'height' =>  $settings['height'],
					'color'  =>   array($settings['chart_color']),
					'colorbox' =>  $settings['chart_colorbox'],
					'markersize' =>  $settings['markersize'],
					'hebrew' =>  $settings['lang'],
				)
			);
			?>

		<script>
			window.onload = new Function("main('mainChart');");
		</script>	
			<div id='myChart-<?php echo $id_gen ?>' ng-app="chartsApp" class="irfeed-chart">
					<div class="chart-container" ng-controller="mainController">
						<div class="mainButtons">
						<?php
						if ( $settings['list'] ) {
							foreach (  $settings['list'] as $item ) {
								?>
								<button class="elementor-repeater-item-<?php echo $item['_id'] ?> <?php echo $item['class_buttons_tabs'] ?>" ng-click="showHideFunc('<?php echo $item['ng-click_buttons_tabs'] ?>');" id="<?php echo $item['id_buttons_tabs'] ?>">
								<?php \Elementor\Icons_Manager::render_icon( $item['icon_buttons_tabs'], [ 'aria-hidden' => 'true' ] ); ?>
								<?php echo $item['head_tabs_text'] ?>
							    </button>
								<?
							}
						}
						?>
						</div>

						<div ng-hide="hideDay">
							<div  id="dayChart"></div>
						</div>
					
						<div ng-hide="hideWeek">
							<div id="weekChart"></div>
						</div>

						<div ng-show="showFiveYear">
							<div id="fiveYearChart"></div>
						</div>
					
						<div id="nodata"></div>
						</div>
					</div>
			</div>

		<?php
	}
}