<?php

class Irfeed_Elementor_Widgets {

	protected static $instance = null;

	public static function get_instance() {
		if ( ! isset( static::$instance ) ) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	protected function __construct() {
		require_once('irfeed_elementor_stock_quote.php');
		require_once('irfeed_elementor_charts.php');
		require_once('irfeed_elementor_pie.php');
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ] );
	}

	public function register_widgets() {
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Irfeed_Stock_Quote() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Irfeed_Charts() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Irfeed_Pie() );
	}

}

add_action( 'init', 'irfeed_elementor_init' );
function irfeed_elementor_init() {
	Irfeed_Elementor_Widgets::get_instance();
}

function irfeed_elementor_widget_categories( $elements_manager ) {

	$elements_manager->add_category(
		'irfeed-category',
		[
			'title' => __( 'IRfeed', 'irfeed' ),
			'icon' => 'fa fa-plug',
		]
	);
}
add_action( 'elementor/elements/categories_registered', 'irfeed_elementor_widget_categories' );