<?php

/* Create Repots as post type */
function irfeed_reports_post_type() {
    register_post_type('reports',
        array(
            'labels'      => array(
                'name'                  => __( 'Reports', 'irfeed'),
                'singular_name'         => __( 'Report', 'irfeed'),
                'menu_name'             => __( 'Reports', 'irfeed' ),
                'name_admin_bar'        => __( 'Reports', 'irfeed' ),
                'archives'              => __( 'Reports Archives', 'irfeed' ),
                'attributes'            => __( 'Reports Attributes', 'irfeed' ),
                'parent_item_colon'     => __( 'Parent Item:', 'irfeed' ),
                'all_items'             => __( 'All Reports', 'irfeed' ),
                'add_new_item'          => __( 'Add New Report', 'irfeed' ),
                'add_new'               => __( 'Add New', 'irfeed' ),
                'new_item'              => __( 'New Report', 'irfeed' ),
                'edit_item'             => __( 'Edit Report', 'irfeed' ),
                'update_item'           => __( 'Update Report', 'irfeed' ),
                'view_item'             => __( 'View Report', 'irfeed' ),
                'view_items'            => __( 'View Reports', 'irfeed' ),
                'search_items'          => __( 'Search Report', 'irfeed' ),
                'not_found'             => __( 'Not found', 'irfeed' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'irfeed' ),
                'featured_image'        => __( 'Featured Image', 'irfeed' ),
                'set_featured_image'    => __( 'Set Report image', 'irfeed' ),
                'remove_featured_image' => __( 'Remove Report image', 'irfeed' ),
                'use_featured_image'    => __( 'Use as Report image', 'irfeed' ),
                'insert_into_item'      => __( 'Insert into Report', 'irfeed' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Report', 'irfeed' ),
                'items_list'            => __( 'Reports list', 'irfeed' ),
                'items_list_navigation' => __( 'Reports list navigation', 'irfeed' ),
                'filter_items_list'     => __( 'Filter Reports list', 'irfeed' ),
            ),
            'public'                => true,
            'has_archive'           => true,
            'hierarchical'          => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'show_in_rest'          => true,
            'supports' => array('title', 'editor', 'post-formats', 'custom-fields')
        )


    );
}
add_action('init', 'irfeed_reports_post_type');

function irfeed_reports_year_taxonomy() {
     $labels = array(
         'name'              => __( 'Reports Year', 'irfeed' ),
         'singular_name'     => __( 'Reports Year', 'irfeed' ),
         'search_items'      => __( 'Search Reports Year', 'irfeed' ),
         'all_items'         => __( 'All Reports Year', 'irfeed' ),
         'parent_item'       => __( 'Parent Reports Year', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Reports Year:', 'irfeed' ),
         'edit_item'         => __( 'Edit Reports Year', 'irfeed' ),
         'update_item'       => __( 'Update Reports Year', 'irfeed' ),
         'add_new_item'      => __( 'Add New Reports Year', 'irfeed' ),
         'new_item_name'     => __( 'New Reports Year', 'irfeed' ),
         'menu_name'         => __( 'Reports Year', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'year' ],
     );
     register_taxonomy( 'year', [ 'reports' ], $args );
}
add_action('init', 'irfeed_reports_year_taxonomy');

function irfeed_reports_month_taxonomy() {
     $labels = array(
         'name'              => __( 'Reports Month', 'irfeed' ),
         'singular_name'     => __( 'Reports Month', 'irfeed' ),
         'search_items'      => __( 'Search Reports Month', 'irfeed' ),
         'all_items'         => __( 'All Reports Month', 'irfeed' ),
         'parent_item'       => __( 'Parent Reports Month', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Reports Month:', 'irfeed' ),
         'edit_item'         => __( 'Edit Reports Month', 'irfeed' ),
         'update_item'       => __( 'Update Reports Month', 'irfeed' ),
         'add_new_item'      => __( 'Add New Reports Month', 'irfeed' ),
         'new_item_name'     => __( 'New Reports Month', 'irfeed' ),
         'menu_name'         => __( 'Reports Month', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'month' ],
     );
     register_taxonomy( 'month', [ 'reports' ], $args );
}
add_action('init', 'irfeed_reports_month_taxonomy');

add_post_type_support( 'reports', 'post-formats' );





function irfeed_childtheme_formats(){
     add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link' ) );
}
add_action( 'after_setup_theme', 'irfeed_childtheme_formats', 11 );

function irfeed_print_post_title() {
	global $post;
	$thePostID = $post->ID;
	$post_id = get_post($thePostID);
	$title = $post_id->post_title;
	$perm = get_permalink($post_id);
	$post_keys = array(); $post_val = array();
	$post_keys = get_post_custom_keys($thePostID);

	if (!empty($post_keys)) {
	foreach ($post_keys as $pkey) {
	if ($pkey=='external_url') {
	$post_val = get_post_custom_values($pkey);
	}
	}
	if (empty($post_val)) {
	$link = $perm;
	} else {
	$link = $post_val[0];
	}
	} else {
	$link = $perm;
	}
	echo '<h2><a href="'.$link.'" rel="bookmark" title="'.$title.'">'.$title.'</a></h2>';
}


add_action( 'rest_api_init', 'add_irfeed_id' );

function add_irfeed_id() {
	$field_name = 'irfeed_id';
    register_rest_field(
    'reports', $field_name,
    array(
       'get_callback'  => function ( $object ) use ( $field_name ) {
		   // Get field as single value from post meta.
		   return get_post_meta( $object['id'], $field_name, true );
	   },
		'update_callback' => function ( $value, $object ) use ( $field_name ) {
			// Update the field/meta value.
			update_post_meta( $object->ID, $field_name, $value );
		},
        'schema' => array(
            'type'        => 'integer'
        ),
        )
    );
}


function update_post_meta_irfeed_id($object, $meta_value) {
	$havemetafield  = get_post_meta($object['id'], 'reports', false);
    if ($havemetafield) {
        $ret = update_post_meta($object['id'], 'irfeed_id', $meta_value );
    } else {
        $ret = add_post_meta( $object['id'], 'irfeed_id', $meta_value ,true );
    }
    return true;
}


function get_irfeed_id( $object, $field_name, $request ) {
    return get_post_meta( $object['id'], $field, true );
}

add_action(
    'rest_api_init',
    function () {
        // Field name to register.
        $field = 'irfeed_link';
        register_rest_field(
            'reports',
            $field,
            array(
                'get_callback'    => function ( $object ) use ( $field ) {
                    // Get field as single value from post meta.
                    return get_post_meta( $object['id'], $field, true );
                },
                'update_callback' => function ( $value, $object ) use ( $field ) {
                    // Update the field/meta value.
                    update_post_meta( $object->ID, $field, $value );
                },
                'schema'          => array(
                    'type'        => 'string',
                ),
            )
        );
    }
);




