<?php


function irfeed_Financials_shortcode($atts = array()) {
    shortcode_atts(array(
        'mode' => null
    ), $atts);

	if(!empty($atts['mode'])) {
		echo '<div id="feed"></div>';
		wp_enqueue_script( 'feed', plugin_dir_url( __DIR__ ) . 'assets/react/Financials.js' );
		wp_localize_script( 'feed', 'api_object',
        	array(
            'siteUrl' => get_site_url(),
       		 )
    	);
    wp_enqueue_script('feed');
	}

}
add_shortcode('feedFinancials', 'irfeed_Financials_shortcode');