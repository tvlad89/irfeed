<?php
namespace Elementor;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;



class Irfeed_Pie extends Widget_Base {

	protected function generateRandomString($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function get_name() {
		return 'irfeed_elementor_pie';
	}
	
	public function get_title() {
		return __( 'Stakeholders Pie', 'irfeed' );
	}
	
	public function get_icon() {
		return 'eicon-adjust';
	}
	
	public function get_categories() {
		return [ 'irfeed-category' ];
	}
	
	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Stakeholders Pie Settings', 'irfeed' ),
			]
		);

		$this->add_control(
			'stock',
			[
				'label' => __( 'Stock Number', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Number', 'irfeed' ),
			]
		);

		$this->add_control(
			'key',
			[
				'label' => __( 'Key', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Key', 'irfeed' ),
			]
		);
		$this->add_control(
			'lang',
			[
				'label' => __( 'Hebrew RTL Chart', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'irfeed' ),
				'label_off' => __( 'No', 'irfeed' ),
				'return_value' => true,
				'default' => false,
			]
		);
		

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Chart Style', 'irfeed' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'table_title_typography',
				'label' => __( 'Table Title Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .mainTableHead',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'table_content_typography',
				'label' => __( 'Table Content Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .mainTableBodyRow',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'pie_legend_typography',
				'label' => __( 'Pie Legend Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .apexcharts-legend',
			]
		);

		$this->add_control(
			'height',
			[
				'label' => __( 'Height', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 5,
				'max' => 700,
				'step' => 1,
				'default' => 350,
			]
		);

		$this->add_control(
			'chart_color',
			[
				'label' => esc_html__( 'Chart Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);

		$this->add_control(
			'chart_colorbox',
			[
				'label' => esc_html__( 'Chart Price Color', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000000',

			]
		);

		$this->add_control(
			'stroke',
			[
				'label' => __( 'Line thickness', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 10,
				'step' => 1,
				'default' => 2,
			]
		);

		$this->add_control(
			'markersize',
			[
				'label' => __( 'Marker size', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 20,
				'step' => 1,
				'default' => 4,
			]
		);
		$this->end_controls_section();
		$this->start_controls_section(
			'pie_colors',
			[
				'label' => __( 'Pie Style', 'irfeed' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'piecolor01',
			[
				'label' => esc_html__( 'Pie color 01', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor02',
			[
				'label' => esc_html__( 'Pie color 02', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor03',
			[
				'label' => esc_html__( 'Pie color 03', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor04',
			[
				'label' => esc_html__( 'Pie color 04', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor05',
			[
				'label' => esc_html__( 'Pie color 05', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor06',
			[
				'label' => esc_html__( 'Pie color 06', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor07',
			[
				'label' => esc_html__( 'Pie color 07', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor08',
			[
				'label' => esc_html__( 'Pie color 08', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor09',
			[
				'label' => esc_html__( 'Pie color 09', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor10',
			[
				'label' => esc_html__( 'Pie color 10', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor11',
			[
				'label' => esc_html__( 'Pie color 11', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor12',
			[
				'label' => esc_html__( 'Pie color 12', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor13',
			[
				'label' => esc_html__( 'Pie color 13', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor14',
			[
				'label' => esc_html__( 'Pie color 14', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor15',
			[
				'label' => esc_html__( 'Pie color 15', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor16',
			[
				'label' => esc_html__( 'Pie color 16', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor17',
			[
				'label' => esc_html__( 'Pie color 17', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor18',
			[
				'label' => esc_html__( 'Pie color 18', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor19',
			[
				'label' => esc_html__( 'Pie color 19', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor20',
			[
				'label' => esc_html__( 'Pie color 20', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->add_control(
			'piecolor21',
			[
				'label' => esc_html__( 'Pie color 21', 'irfeed' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',

			]
		);
		$this->end_controls_section();
	}

	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );

	}

	protected function render( $data = [], $args = null ) {
			parent::render( $data, $args );
			$settings = $this->get_settings_for_display();
			$id_gen = $this->generateRandomString();
			wp_enqueue_script( 'irfeed-elementor-main', plugin_dir_url( __DIR__ ) . 'assets/js/main.js' );
			wp_enqueue_script( 'irfeed-elementor-pie', plugin_dir_url( __DIR__ ) . 'assets/js/pieInterestedParties.js' );
			wp_enqueue_style( 'irfeed-elementor-charts', plugin_dir_url( __DIR__ ) . 'assets/css/pie.css' );
			
			wp_localize_script( 'irfeed-elementor-main', 'api_object',
				array(
					'apiKey' => $settings['key'],
					'stockNumber' => $settings['stock'],
					'mode' =>  $settings['mode'],
					'type' =>  $settings['type'],
					'stroke' =>  $settings['stroke'],
					'height' =>  $settings['height'],
					'color'  =>   array($settings['chart_color']),
					'colorbox' =>  $settings['chart_colorbox'],
					'markersize' =>  $settings['markersize'],
					'parties_chart_hebrew' =>  $settings['lang'],
					'parties_table_hebrew' =>  $settings['lang'],
					'piecolor01' =>  $settings['piecolor01'],
					'piecolor02' =>  $settings['piecolor02'],
					'piecolor03' =>  $settings['piecolor03'],
					'piecolor04' =>  $settings['piecolor04'],
					'piecolor05' =>  $settings['piecolor05'],
					'piecolor06' =>  $settings['piecolor06'],
					'piecolor07' =>  $settings['piecolor07'],
					'piecolor08' =>  $settings['piecolor08'],
					'piecolor09' =>  $settings['piecolor09'],
					'piecolor10' =>  $settings['piecolor10'],
					'piecolor11' =>  $settings['piecolor11'],
					'piecolor12' =>  $settings['piecolor12'],
					'piecolor13' =>  $settings['piecolor13'],
					'piecolor14' =>  $settings['piecolor14'],
					'piecolor15' =>  $settings['piecolor15'],
					'piecolor16' =>  $settings['piecolor16'],
					'piecolor17' =>  $settings['piecolor17'],
					'piecolor18' =>  $settings['piecolor18'],
					'piecolor19' =>  $settings['piecolor19'],
					'piecolor20' =>  $settings['piecolor20'],
					'piecolor21' =>  $settings['piecolor21'],
					
				)
			);
			?>

		<script>
			window.onload = new Function("main('pie_interested_parties');");
		</script>
	<div class="irfeed-pie">	
		<div id='nodata'></div>
    	<div id='nodedata_buttons'></div>
    	<div class='pieChartDiv' id="pie_interested_parties"></div>
    	<div class='TableDiv'></div>
	</div>

	<?php
	}
}