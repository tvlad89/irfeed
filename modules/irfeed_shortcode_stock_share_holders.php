<?php


class StockQuote {

    public $data;

    public  function initial(){

        $api_key = get_option("api_key");
        $companies_number = get_option("companies_number");
        $stock_number = explode(",", $companies_number);
        $http_query = http_build_query(array('stock_number' => $stock_number), '', '&');
        $url = "https://finance.irfeed.co.il/api/getStocks?{$http_query}&api_key={$api_key}";
        $response = wp_remote_post( $url, array(
                'method' => 'GET',
                'timeout' => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => array(),
                'body' => array( ),
                'cookies' => array()
            )
        );

        if ( is_wp_error( $response ) ) {
            $error_message = $response->get_error_message();
            return "NO DATA"; // TODO
        } else {
            $data = json_decode($response['body']);
            if(isset($data->error_message) || isset($data->error_type) || isset($data->error)){
                $this->data = "ERROR";
            }

            $keys_arr = [
                'Rate',
                'BaseRate',
                'BaseRateChangePercentage',
                'BaseRateChange',
                'DailyHighestRate',
                'DailyLowestRate',
                'HebName',
                'EngName',
                'HebSymbol',
                'EngSymbol',
                'Exchange',
                'Symbol',
                'AllYearMaximumRate',
                'AllYearMinimumRate',
                'LastDealDate',
                'LastDealRate',
                'GrossYieldToMaturity',
                'GrossDuration',
                'ListedFortune',
                'LastKnownRate',
                'LastKnownRateDate',
                'LastKnownRateTime',
                'DailyTurnover',
                'Duality',
                'CompanyNum',
                'CorpNum',
                'CompanyMarketValue',
                'MarketWorth',
                'CurrencyCode',
                'TradingStage',
                'AllMonthMinimumRate',
                'AllMonthMaximumRate',
                'AllMonthAverageNIS',
                'QuarterMinimumRate',
                'QuarterMaximumRate',
                'QuarterAverageNIS',
                'UpToOneYearMinimumRate',
                'UpToOneYearMaximumRate',
                'UpToOneYearAverageNIS',
                'AllYearAverageNIS',
                'DailyNISRevenue'
            ];
            $arr_obj = [];
            $key = '-Key';
            foreach ($data as $obj){
                $obj_temp = new stdClass();
                foreach ($keys_arr as $field){
                    $obj_temp->{$field} = $this->initialValue($obj->{$field});
                }
                $arr_obj[$obj->stock_number] = $obj_temp;
            }
            $this->data = $arr_obj;
        }
    }

    public function initialValue($value){
        $temp_value = $this->data == 'ERROR' || empty($value) ? '0' : $value;
        return $temp_value;
    }

    public function initialClassname($val){
        $class_name = '';
        if($val != '0' && $val > 0){
            $class_name = "class=dice-data-up";
        }elseif($val != '0' && $val < 0){
            $class_name = "class=dice-data-down";
        }
        return $class_name;
    }

    public function __construct()
    {
        $this->initial();
    }

    public function addShortCode($short_code_name, $function_name){
        add_shortcode( $short_code_name, array($this, $function_name) );
    }
/* 001*/
    function get_shareholder_table($atts = array(), $content = null, $tag){
        shortcode_atts(array(
            'stock' => ' '
        ), $atts);
        return "<div dir='ltr'><label>{$this->data[$atts['stock']]->Rate}</label></div>";
    }
}
/* Create object of StockQuote class for create short codes of any value*/
$stock_obj = new StockQuote();
/* 001*/
$stock_obj->addShortCode('shareholder_table', 'get_shareholder_table');
