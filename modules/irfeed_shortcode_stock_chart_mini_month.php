<?php

function irfeed_chart_mini_month_shortcode($atts = array()) {
    shortcode_atts(array(
        'stock' => ' '
    ), $atts);
	$stocks_numbers = explode(",", get_option('companies_number'));
	if(!in_array($atts['stock'], $stocks_numbers)){
        return "<div class='nodata-text'>Access denied. Incorrect Api Key or Stock Number.</div>";
    }
    wp_enqueue_script( 'dice-main', plugins_url( '/assets/js/main.js', __DIR__ ));
    wp_enqueue_script( 'dice-mini', plugins_url( '/assets/js/miniChart.js', __DIR__ ));

    wp_localize_script( 'dice-main', 'api_object',
        array(
            'apiKey' => get_option("api_key"),
            'stockNumber' => $atts['stock'],
            'mode' =>  $atts['mode'],
            'type' =>  $atts['type'],
            'stroke' =>  $atts['stroke'],
            'height' =>  $atts['height'],
            'color' =>  isset($atts['color']) ? explode(",", $atts['color']) : ['#000'],
            'colorbox' =>  $atts['colorbox'],
            'markersize' =>  $atts['markersize'],
        )
    );

    return '<body onload="main(\'miniChart2\')">
    <div id="nodata"></div>
    <div  id="miniChart2"></div>
     ';
}
add_shortcode('dicechart_mini_month', 'irfeed_chart_mini_month_shortcode');