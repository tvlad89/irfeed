<?php

/* Create Repots as post type */
function irfeed_meetings_post_type() {
    register_post_type('meetings',
        array(
            'labels'      => array(
                'name'                  => __( 'Meetings', 'irfeed'),
                'singular_name'         => __( 'Meeting', 'irfeed'),
                'menu_name'             => __( 'Meetings', 'irfeed' ),
                'name_admin_bar'        => __( 'Meetings', 'irfeed' ),
                'archives'              => __( 'Meetings Archives', 'irfeed' ),
                'attributes'            => __( 'Meetings Attributes', 'irfeed' ),
                'parent_item_colon'     => __( 'Parent Item:', 'irfeed' ),
                'all_items'             => __( 'All Meetings', 'irfeed' ),
                'add_new_item'          => __( 'Add New Meeting', 'irfeed' ),
                'add_new'               => __( 'Add New', 'irfeed' ),
                'new_item'              => __( 'New Meeting', 'irfeed' ),
                'edit_item'             => __( 'Edit Meeting', 'irfeed' ),
                'update_item'           => __( 'Update Meeting', 'irfeed' ),
                'view_item'             => __( 'View Meeting', 'irfeed' ),
                'view_items'            => __( 'View Meetings', 'irfeed' ),
                'search_items'          => __( 'Search Meeting', 'irfeed' ),
                'not_found'             => __( 'Not found', 'irfeed' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'irfeed' ),
                'featured_image'        => __( 'Featured Image', 'irfeed' ),
                'set_featured_image'    => __( 'Set Meeting image', 'irfeed' ),
                'remove_featured_image' => __( 'Remove Meeting image', 'irfeed' ),
                'use_featured_image'    => __( 'Use as Meeting image', 'irfeed' ),
                'insert_into_item'      => __( 'Insert into Meeting', 'irfeed' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Meeting', 'irfeed' ),
                'items_list'            => __( 'Meetings list', 'irfeed' ),
                'items_list_navigation' => __( 'Meetings list navigation', 'irfeed' ),
                'filter_items_list'     => __( 'Filter Meetings list', 'irfeed' ),
            ),
            'public'                => true,
            'has_archive'           => true,
            'hierarchical'          => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'show_in_rest'          => true,
            'supports' => array('title', 'editor', 'post-formats', 'custom-fields')
        )


    );
}
add_action('init', 'irfeed_meetings_post_type');

function irfeed_meetings_year_taxonomy() {
     $labels = array(
         'name'              => __( 'Meetings Year', 'irfeed' ),
         'singular_name'     => __( 'Meetings Year', 'irfeed' ),
         'search_items'      => __( 'Search Meetings Year', 'irfeed' ),
         'all_items'         => __( 'All Meetings Year', 'irfeed' ),
         'parent_item'       => __( 'Parent Meetings Year', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Meetings Year:', 'irfeed' ),
         'edit_item'         => __( 'Edit Meetings Year', 'irfeed' ),
         'update_item'       => __( 'Update Meetings Year', 'irfeed' ),
         'add_new_item'      => __( 'Add New Meetings Year', 'irfeed' ),
         'new_item_name'     => __( 'New Meetings Year', 'irfeed' ),
         'menu_name'         => __( 'Meetings Year', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'year' ],
     );
     register_taxonomy( 'year', [ 'Meetings' ], $args );
}
add_action('init', 'irfeed_meetings_year_taxonomy');

function irfeed_meetings_month_taxonomy() {
     $labels = array(
         'name'              => __( 'Meetings Month', 'irfeed' ),
         'singular_name'     => __( 'Meetings Month', 'irfeed' ),
         'search_items'      => __( 'Search Meetings Month', 'irfeed' ),
         'all_items'         => __( 'All Meetings Month', 'irfeed' ),
         'parent_item'       => __( 'Parent Meetings Month', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Meetings Month:', 'irfeed' ),
         'edit_item'         => __( 'Edit Meetings Month', 'irfeed' ),
         'update_item'       => __( 'Update Meetings Month', 'irfeed' ),
         'add_new_item'      => __( 'Add New Meetings Month', 'irfeed' ),
         'new_item_name'     => __( 'New Meetings Month', 'irfeed' ),
         'menu_name'         => __( 'Meetings Month', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'month' ],
     );
     register_taxonomy( 'month', [ 'Meetings' ], $args );
}
add_action('init', 'irfeed_meetings_month_taxonomy');

add_post_type_support( 'meetings', 'post-formats' );








