<?php

/* Create Repots as post type */
function irfeed_financials_post_type() {
    register_post_type('financials',
        array(
            'labels'      => array(
                'name'                  => __( 'Financials', 'irfeed'),
                'singular_name'         => __( 'Financial', 'irfeed'),
                'menu_name'             => __( 'Financials', 'irfeed' ),
                'name_admin_bar'        => __( 'Financials', 'irfeed' ),
                'archives'              => __( 'Financials Archives', 'irfeed' ),
                'attributes'            => __( 'Financials Attributes', 'irfeed' ),
                'parent_item_colon'     => __( 'Parent Item:', 'irfeed' ),
                'all_items'             => __( 'All Financials', 'irfeed' ),
                'add_new_item'          => __( 'Add New Financial', 'irfeed' ),
                'add_new'               => __( 'Add New', 'irfeed' ),
                'new_item'              => __( 'New Financial', 'irfeed' ),
                'edit_item'             => __( 'Edit Financial', 'irfeed' ),
                'update_item'           => __( 'Update Financial', 'irfeed' ),
                'view_item'             => __( 'View Financial', 'irfeed' ),
                'view_items'            => __( 'View Financials', 'irfeed' ),
                'search_items'          => __( 'Search Financial', 'irfeed' ),
                'not_found'             => __( 'Not found', 'irfeed' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'irfeed' ),
                'featured_image'        => __( 'Featured Image', 'irfeed' ),
                'set_featured_image'    => __( 'Set Financial image', 'irfeed' ),
                'remove_featured_image' => __( 'Remove Financial image', 'irfeed' ),
                'use_featured_image'    => __( 'Use as Financial image', 'irfeed' ),
                'insert_into_item'      => __( 'Insert into Financial', 'irfeed' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Financial', 'irfeed' ),
                'items_list'            => __( 'Financials list', 'irfeed' ),
                'items_list_navigation' => __( 'Financials list navigation', 'irfeed' ),
                'filter_items_list'     => __( 'Filter Financials list', 'irfeed' ),
            ),
            'public'                => true,
            'has_archive'           => true,
            'hierarchical'          => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'show_in_rest'          => true,
            'supports' => array('title', 'editor', 'post-formats', 'custom-fields')
        )


    );
}
add_action('init', 'irfeed_financials_post_type');

function irfeed_financials_year_taxonomy() {
     $labels = array(
         'name'              => __( 'Financials Year', 'irfeed' ),
         'singular_name'     => __( 'Financials Year', 'irfeed' ),
         'search_items'      => __( 'Search Financials Year', 'irfeed' ),
         'all_items'         => __( 'All Financials Year', 'irfeed' ),
         'parent_item'       => __( 'Parent Financials Year', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Financials Year:', 'irfeed' ),
         'edit_item'         => __( 'Edit Financials Year', 'irfeed' ),
         'update_item'       => __( 'Update Financials Year', 'irfeed' ),
         'add_new_item'      => __( 'Add New Financials Year', 'irfeed' ),
         'new_item_name'     => __( 'New Financials Year', 'irfeed' ),
         'menu_name'         => __( 'Financials Year', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'year' ],
     );
     register_taxonomy( 'year', [ 'Financials' ], $args );
}
add_action('init', 'irfeed_financials_year_taxonomy');

function irfeed_financials_month_taxonomy() {
     $labels = array(
         'name'              => __( 'Financials Month', 'irfeed' ),
         'singular_name'     => __( 'Financials Month', 'irfeed' ),
         'search_items'      => __( 'Search Financials Month', 'irfeed' ),
         'all_items'         => __( 'All Financials Month', 'irfeed' ),
         'parent_item'       => __( 'Parent Financials Month', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Financials Month:', 'irfeed' ),
         'edit_item'         => __( 'Edit Financials Month', 'irfeed' ),
         'update_item'       => __( 'Update Financials Month', 'irfeed' ),
         'add_new_item'      => __( 'Add New Financials Month', 'irfeed' ),
         'new_item_name'     => __( 'New Financials Month', 'irfeed' ),
         'menu_name'         => __( 'Financials Month', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'month' ],
     );
     register_taxonomy( 'month', [ 'Financials' ], $args );
}
add_action('init', 'irfeed_financials_month_taxonomy');

add_post_type_support( 'financials', 'post-formats' );








