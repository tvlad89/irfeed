<?php
namespace Elementor;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;

class Irfeed_Stock_Quote extends Widget_Base {
	
	public function get_name() {
		return 'irfeed_elementor_stock_quote';
	}
	
	public function get_title() {
		return __( 'Stock Quote', 'irfeed' );
	}
	
	public function get_icon() {
		return 'eicon-exchange';
	}
	
	public function get_categories() {
		return [ 'irfeed-category' ];
	}
	
	public function get_script_depends() {
		return [ 'irfeed' ];
	}


	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Stock Number', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Number', 'irfeed' ),
			]
		);

		$this->add_control(
			'stock-field',
			[
				'label' => __( 'Field', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'last_known_rate',
				'options' => [
					'rate'  => __( 'Rate', 'irfeed' ),
					'base_rate' => __( 'Base Rate', 'irfeed' ),
					'base_rate_change_percentage' => __( 'Base Rate Change Percentage', 'irfeed' ),
					'base_rate_change' => __( 'Base Rate Change', 'irfeed' ),
					'daily_highest_rate' => __( 'Daily Highest Rate', 'irfeed' ),
					'daily_lowest_rate' => __( 'Daily Lowest Rate', 'irfeed' ),
					'heb_name' => __( 'Hebrew Name', 'irfeed' ),
					'eng_name' => __( 'English Name', 'irfeed' ),
					'heb_symbol' => __( 'Hebrew Symbol', 'irfeed' ),
					'eng_symbol' => __( 'English Symbol', 'irfeed' ),
					'exchange' => __( 'Exchange', 'irfeed' ),
					'symbol' => __( 'Symbol', 'irfeed' ),
					'all_year_maximum_rate' => __( 'All Year Maximum Rate', 'irfeed' ),
					'all_year_minimum_rate' => __( 'All Year Minimum Rate', 'irfeed' ),
					'last_deal_date' => __( 'Last Deal Date', 'irfeed' ),
					'last_deal_rate' => __( 'Last Deal Rate', 'irfeed' ),
					'gross_yield_to_maturity' => __( 'Gross Yield To Maturity', 'irfeed' ),
					'gross_duration' => __( 'Gross Duration', 'irfeed' ),
					'listed_fortune' => __( 'Listed Fortune', 'irfeed' ),
					'last_known_rate' => __( 'Last Known Rate', 'irfeed' ),
					'last_Known_Rate_date' => __( 'Last Known Rate Date', 'irfeed' ),
					'last_Known_Rate_time' => __( 'Last Known Rate Time', 'irfeed' ),
					'daily_turnover' => __( 'Daily Turnover', 'irfeed' ),
					'duality' => __( 'Duality', 'irfeed' ),
					'company_num' => __( 'Company Number', 'irfeed' ),
					'corp_num' => __( 'Corp Number', 'irfeed' ),
					'company_market_value' => __( 'Company Market Value', 'irfeed' ),
					'market_worth' => __( 'Market Worth', 'irfeed' ),
					'currency_code' => __( 'Currency Code', 'irfeed' ),
					'trading_stage' => __( 'Trading Stage', 'irfeed' ),
					'all_month_minimum_rate' => __( 'All Month Minimum Rate', 'irfeed' ),
					'all_month_maximum_rate' => __( 'All Month Maximum Rate', 'irfeed' ),
					'all_month_average_nis' => __( 'All Month Average NIS', 'irfeed' ),
					'quarter_minimum_rate' => __( 'Quarter Minimum Rate', 'irfeed' ),
					'quarter_maximum_rate' => __( 'Quarter Maximum Rate', 'irfeed' ),
					'quarter_average_nis' => __( 'Quarter Average NIS', 'irfeed' ),
					'up_to_one_year_minimum_rate' => __( 'Up To One Year Minimum Rate', 'irfeed' ),
					'up_to_one_year_maximum_rate' => __( 'Up To One Year Maximum Rate', 'irfeed' ),
					'up_to_one_year_average_nis' => __( 'Up To One Year Average NIS', 'irfeed' ),
					'all_year_average_nis' => __( 'All Year Average NIS', 'irfeed' ),
					'daily_nis_revenue' => __( 'Daily NIS Revenue', 'irfeed' ),
				],
			]
		);

		$this->add_control(
			'before-text',
			[
				'label' => __( 'Before Text', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'after-text',
			[
				'label' => __( 'After Text', 'irfeed' ),
				'type' => Controls_Manager::TEXT,
			]
		);
		$this->add_control(
			'before-symbol',
			[
				'label' => __( 'Before Symbol', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					''  => __( 'No', 'irfeed' ),
					'₪'  => __( 'ILS ₪', 'irfeed' ),
					'$'  => __( 'USD $', 'irfeed' ),
				],
			]
		);
		$this->add_control(
			'after-symbol',
			[
				'label' => __( 'After Symbol', 'irfeed' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					''  => __( 'No', 'irfeed' ),
					'₪'  => __( 'ILS ₪', 'irfeed' ),
					'$'  => __( 'USD $', 'irfeed' ),
				],
			]
		);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Style', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'text_align',
			[
				'label' => __( 'Alignment', 'elementor' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'plugin-domain' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'plugin-domain' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'plugin-domain' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'toggle' => true,
			]
		);
		
				
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Text Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'global' => [
					'default' => Global_Colors::COLOR_PRIMARY,
				],
				'selectors' => [
					'{{WRAPPER}} .irfeed-value' => 'color: {{VALUE}};',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'content_typography',
				'label' => __( 'Typography', 'elementor' ),
				'selector' => '{{WRAPPER}} .irfeed-value',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'label' => __( 'Text Shadow', 'elementor' ),
				'selector' => '{{WRAPPER}} .irfeed-value',
			]
		);


		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
        echo '<div class="irfeed-value" style="text-align: ' . $settings['text_align'] . '">';
		echo $settings['before-text'];
		echo $settings['before-symbol'];
		    $short = '['.$settings['stock-field'].' , stock="'.$settings['title'].'"]';
        echo do_shortcode($short); 
		echo $settings['after-symbol'];
		echo $settings['after-text'];
        echo '</div>';
	}
}