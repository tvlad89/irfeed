<?php

function irfeed_chart_shortcode($atts = array()) {
    shortcode_atts(array(
        'stock' => ' '
    ), $atts);
	$stocks_numbers = explode(",", get_option('companies_number'));
	if(!in_array($atts['stock'], $stocks_numbers)){
        return "<div class='nodata-text'>Access denied. Incorrect Api Key or Stock Number.</div>";
    }

    wp_enqueue_script( 'dice-main', plugins_url( '/assets/js/main.js', __DIR__ ));

    wp_localize_script( 'dice-main', 'api_object',
        array(
            'apiKey' => get_option("api_key"),
            'stockNumber' => $atts['stock'],
            'mode' =>  $atts['mode'],
            'type' =>  $atts['type'],
            'stroke' =>  $atts['stroke'],
            'height' =>  $atts['height'],
            'color' =>  isset($atts['color']) ? explode(",", $atts['color']) : ['#000'],
            'colorbox' =>  $atts['colorbox'],
            'markersize' =>  $atts['markersize'],
        )
    );

    return '<body ng-app="chartsApp" onload="main(\'mainChart\')">
    <div ng-controller="mainController">

    <div class="chart-container shortcode-chart">
        <div class="mainButtons">
            <button class="btn-ideadice" ng-click="showHideFunc(\'showDay\');" id="one_day">1D</button>
            <button class="btn-ideadice" ng-click="showHideFunc(\'showWeek\');" id="one_week">1W</button>
            <button class="btn-ideadice" ng-click="showHideFunc(\'showFiveYear\');" id="one_month">1M</button>
            <button class="btn-ideadice" ng-click="showHideFunc(\'showFiveYear\');" id="three_month">3M</button>
            <button class="btn-ideadice" ng-click="showHideFunc(\'showFiveYear\');" id="six_month">6M</button>
            <button class="btn-ideadice active" id="one_year" ng-click="showHideFunc(\'showFiveYear\');">1Y</button>
            <button class="btn-ideadice" ng-click="showHideFunc(\'showFiveYear\');"  id="three_year">3Y</button>
            <button class="btn-ideadice" ng-click="showHideFunc(\'showFiveYear\');"  id="five_year">5Y</button>
        </div>

        <div ng-hide="hideDay">
            <div  id="dayChart"></div>
        </div>
        
        <div ng-hide="hideWeek">
            <div  id="weekChart"></div>
        </div>

        <div ng-show="showFiveYear">
            <div id="fiveYearChart"></div>
        </div>
        
        <div id="nodata"></div>
        
    </div>

    <!--END ang  mainController-->
    </div>
     ';
}
add_shortcode('dicechart', 'irfeed_chart_shortcode');