<?php

/* Create Repots as post type */
function irfeed_analysts_post_type() {
    register_post_type('analysts',
        array(
            'labels'      => array(
                'name'                  => __( 'Analysts', 'irfeed'),
                'singular_name'         => __( 'Analyst', 'irfeed'),
                'menu_name'             => __( 'Analysts', 'irfeed' ),
                'name_admin_bar'        => __( 'Analysts', 'irfeed' ),
                'archives'              => __( 'Analysts Archives', 'irfeed' ),
                'attributes'            => __( 'Analysts Attributes', 'irfeed' ),
                'parent_item_colon'     => __( 'Parent Item:', 'irfeed' ),
                'all_items'             => __( 'All Analysts', 'irfeed' ),
                'add_new_item'          => __( 'Add New Analyst', 'irfeed' ),
                'add_new'               => __( 'Add New', 'irfeed' ),
                'new_item'              => __( 'New Analyst', 'irfeed' ),
                'edit_item'             => __( 'Edit Analyst', 'irfeed' ),
                'update_item'           => __( 'Update Analyst', 'irfeed' ),
                'view_item'             => __( 'View Analyst', 'irfeed' ),
                'view_items'            => __( 'View Analysts', 'irfeed' ),
                'search_items'          => __( 'Search Analyst', 'irfeed' ),
                'not_found'             => __( 'Not found', 'irfeed' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'irfeed' ),
                'featured_image'        => __( 'Featured Image', 'irfeed' ),
                'set_featured_image'    => __( 'Set Analyst image', 'irfeed' ),
                'remove_featured_image' => __( 'Remove Analyst image', 'irfeed' ),
                'use_featured_image'    => __( 'Use as Analyst image', 'irfeed' ),
                'insert_into_item'      => __( 'Insert into Analyst', 'irfeed' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Analyst', 'irfeed' ),
                'items_list'            => __( 'Analysts list', 'irfeed' ),
                'items_list_navigation' => __( 'Analysts list navigation', 'irfeed' ),
                'filter_items_list'     => __( 'Filter Analysts list', 'irfeed' ),
            ),
            'public'                => true,
            'has_archive'           => true,
            'hierarchical'          => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'show_in_rest'          => true,
            'supports' => array('title', 'editor', 'post-formats', 'custom-fields')
        )


    );
}
add_action('init', 'irfeed_analysts_post_type');

function irfeed_analysts_year_taxonomy() {
     $labels = array(
         'name'              => __( 'Analysts Year', 'irfeed' ),
         'singular_name'     => __( 'Analysts Year', 'irfeed' ),
         'search_items'      => __( 'Search Analysts Year', 'irfeed' ),
         'all_items'         => __( 'All Analysts Year', 'irfeed' ),
         'parent_item'       => __( 'Parent Analysts Year', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Analysts Year:', 'irfeed' ),
         'edit_item'         => __( 'Edit Analysts Year', 'irfeed' ),
         'update_item'       => __( 'Update Analysts Year', 'irfeed' ),
         'add_new_item'      => __( 'Add New Analysts Year', 'irfeed' ),
         'new_item_name'     => __( 'New Analysts Year', 'irfeed' ),
         'menu_name'         => __( 'Analysts Year', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'year' ],
     );
     register_taxonomy( 'year', [ 'Analysts' ], $args );
}
add_action('init', 'irfeed_analysts_year_taxonomy');

function irfeed_analysts_month_taxonomy() {
     $labels = array(
         'name'              => __( 'Analysts Month', 'irfeed' ),
         'singular_name'     => __( 'Analysts Month', 'irfeed' ),
         'search_items'      => __( 'Search Analysts Month', 'irfeed' ),
         'all_items'         => __( 'All Analysts Month', 'irfeed' ),
         'parent_item'       => __( 'Parent Analysts Month', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Analysts Month:', 'irfeed' ),
         'edit_item'         => __( 'Edit Analysts Month', 'irfeed' ),
         'update_item'       => __( 'Update Analysts Month', 'irfeed' ),
         'add_new_item'      => __( 'Add New Analysts Month', 'irfeed' ),
         'new_item_name'     => __( 'New Analysts Month', 'irfeed' ),
         'menu_name'         => __( 'Analysts Month', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'month' ],
     );
     register_taxonomy( 'month', [ 'Analysts' ], $args );
}
add_action('init', 'irfeed_analysts_month_taxonomy');

add_post_type_support( 'analysts', 'post-formats' );

add_action( 'rest_api_init', 'add_contributor' );

function add_contributor() {
	$field_name = 'Contributor';
    register_rest_field(
    'analysts', $field_name,
    array(
       'get_callback'  => function ( $object ) use ( $field_name ) {
		   // Get field as single value from post meta.
		   return get_post_meta( $object['id'], $field_name, true );
	   },
		'update_callback' => function ( $value, $object ) use ( $field_name ) {
			// Update the field/meta value.
			update_post_meta( $object->ID, $field_name, $value );
		}
        )
    );
}






