<?php

/* Create Repots as post type */
function irfeed_events_post_type() {
    register_post_type('events',
        array(
            'labels'      => array(
                'name'                  => __( 'Events', 'irfeed'),
                'singular_name'         => __( 'Event', 'irfeed'),
                'menu_name'             => __( 'Events', 'irfeed' ),
                'name_admin_bar'        => __( 'Events', 'irfeed' ),
                'archives'              => __( 'Events Archives', 'irfeed' ),
                'attributes'            => __( 'Events Attributes', 'irfeed' ),
                'parent_item_colon'     => __( 'Parent Item:', 'irfeed' ),
                'all_items'             => __( 'All Events', 'irfeed' ),
                'add_new_item'          => __( 'Add New Event', 'irfeed' ),
                'add_new'               => __( 'Add New', 'irfeed' ),
                'new_item'              => __( 'New Event', 'irfeed' ),
                'edit_item'             => __( 'Edit Event', 'irfeed' ),
                'update_item'           => __( 'Update Event', 'irfeed' ),
                'view_item'             => __( 'View Event', 'irfeed' ),
                'view_items'            => __( 'View Events', 'irfeed' ),
                'search_items'          => __( 'Search Event', 'irfeed' ),
                'not_found'             => __( 'Not found', 'irfeed' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'irfeed' ),
                'featured_image'        => __( 'Featured Image', 'irfeed' ),
                'set_featured_image'    => __( 'Set Event image', 'irfeed' ),
                'remove_featured_image' => __( 'Remove Event image', 'irfeed' ),
                'use_featured_image'    => __( 'Use as Event image', 'irfeed' ),
                'insert_into_item'      => __( 'Insert into Event', 'irfeed' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Event', 'irfeed' ),
                'items_list'            => __( 'Events list', 'irfeed' ),
                'items_list_navigation' => __( 'Events list navigation', 'irfeed' ),
                'filter_items_list'     => __( 'Filter Events list', 'irfeed' ),
            ),
            'public'                => true,
            'has_archive'           => true,
            'hierarchical'          => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'show_in_rest'          => true,
            'supports' => array('title', 'editor', 'post-formats', 'custom-fields')
        )


    );
}
add_action('init', 'irfeed_events_post_type');

function irfeed_events_year_taxonomy() {
     $labels = array(
         'name'              => __( 'Events Year', 'irfeed' ),
         'singular_name'     => __( 'Events Year', 'irfeed' ),
         'search_items'      => __( 'Search Events Year', 'irfeed' ),
         'all_items'         => __( 'All Events Year', 'irfeed' ),
         'parent_item'       => __( 'Parent Events Year', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Events Year:', 'irfeed' ),
         'edit_item'         => __( 'Edit Events Year', 'irfeed' ),
         'update_item'       => __( 'Update Events Year', 'irfeed' ),
         'add_new_item'      => __( 'Add New Events Year', 'irfeed' ),
         'new_item_name'     => __( 'New Events Year', 'irfeed' ),
         'menu_name'         => __( 'Events Year', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'year' ],
     );
     register_taxonomy( 'year', [ 'Events' ], $args );
}
add_action('init', 'irfeed_events_year_taxonomy');

function irfeed_events_month_taxonomy() {
     $labels = array(
         'name'              => __( 'Events Month', 'irfeed' ),
         'singular_name'     => __( 'Events Month', 'irfeed' ),
         'search_items'      => __( 'Search Events Month', 'irfeed' ),
         'all_items'         => __( 'All Events Month', 'irfeed' ),
         'parent_item'       => __( 'Parent Events Month', 'irfeed' ),
         'parent_item_colon' => __( 'Parent Events Month:', 'irfeed' ),
         'edit_item'         => __( 'Edit Events Month', 'irfeed' ),
         'update_item'       => __( 'Update Events Month', 'irfeed' ),
         'add_new_item'      => __( 'Add New Events Month', 'irfeed' ),
         'new_item_name'     => __( 'New Events Month', 'irfeed' ),
         'menu_name'         => __( 'Events Month', 'irfeed' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'show_in_rest'      => true,
         'rewrite'           => [ 'slug' => 'month' ],
     );
     register_taxonomy( 'month', [ 'Events' ], $args );
}
add_action('init', 'irfeed_events_month_taxonomy');

add_post_type_support( 'events', 'post-formats' );








