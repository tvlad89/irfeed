<?php
/**
* Plugin Name: IRfeed Tools
* Plugin URI: https://irfeed.co.il/
* Description: A set of tools for displaying stock market data on WordPress Website
* Author: irfeed team
* Author URI: https://irfeed.co.il/
* Version: 2.0.0
* Text Domain: irfeed
**/

// TODO Shay - build autoloader.
$dir = plugin_dir_path( __FILE__ );

require( $dir . 'admin/irfeed_admin.php');

require( $dir . 'modules/irfeed_pt_reports.php');
require( $dir . 'modules/irfeed_pt_financials.php');
require( $dir . 'modules/irfeed_pt_events.php');
require( $dir . 'modules/irfeed_pt_meetings.php');
require( $dir . 'modules/irfeed_pt_analysts.php');

require( $dir . 'modules/irfeed_shortcode_reports.php');

require( $dir . 'modules/irfeed_shortcode_stock_quote.php');
require( $dir . 'modules/irfeed_shortcode_stock_chart.php');
require( $dir . 'modules/irfeed_shortcode_stock_chart_mini_day.php');
require( $dir . 'modules/irfeed_shortcode_stock_chart_mini_month.php');




require( $dir . 'modules/irfeed_extend_elementor.php');

require( $dir . 'page_links_to/functions.php');
require( $dir . 'page_links_to/page_links_to_plugin.php');
new irfeed_PageLinksTo( __FILE__ );


function irfeed_load_scripts_front() {
     /*  Register JS files    */

    wp_enqueue_script( 'irfeed-angular', plugins_url( '/assets/lib/angular.min.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-apex', plugins_url( '/assets/lib/apexcharts.js', __FILE__ ), array('jquery'));
    wp_enqueue_script( 'irfeed-fontawesome', plugins_url( '/assets/lib/fontawesome.js', __FILE__ ));

    wp_enqueue_script( 'irfeed-angular-app', plugins_url( '/assets/js/ang_app.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-buildchart', plugins_url( '/assets/js/buildchart.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-chartConstructor', plugins_url( '/assets/js/chartConstructor.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-chartSetup', plugins_url( '/assets/js/chartSetup.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-chartSetupDay', plugins_url( '/assets/js/chartSetupDay.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-chartSetupWeek', plugins_url( '/assets/js/chartSetupWeek.js', __FILE__ ));
    wp_enqueue_script( 'irfeed-general_lib', plugins_url( '/assets/js/general_lib.js', __FILE__ ));
    //wp_enqueue_script( 'irfeed-react-reports', plugins_url( '/assets/react/static/js/main.4e3c971d.js', __FILE__ ));

    /*   Register css files   */
    wp_register_style( 'irfeed-design', plugins_url( '/assets/css/design.css', __FILE__ ));

    /*   Call js files */
    wp_enqueue_script('irfeed-angular');
    wp_enqueue_script('irfeed-apex');
    wp_enqueue_script('irfeed-fontawesome');

    wp_enqueue_script('irfeed-angular-app');
    wp_enqueue_script('irfeed-buildchart');
    wp_enqueue_script('irfeed-chartConstructor');
    wp_enqueue_script('irfeed-chartSetup');
    wp_enqueue_script('irfeed-chartSetupDay');
    wp_enqueue_script('irfeed-chartSetupWeek');
    wp_enqueue_script('irfeed-general_lib');
    wp_enqueue_script('irfeed-react-reports');

    wp_enqueue_script('irfeed-mini');


    /*   Call css files */
    wp_enqueue_style( 'irfeed-design' );
}


add_action( 'wp_enqueue_scripts', 'irfeed_load_scripts_front' );



function irfeed_load_lang() {
	load_plugin_textdomain( 'irfeed', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'irfeed_load_lang' );

function irfeed_load_scripts_admin() {
    /*   Register css files */
    wp_register_style( 'irfeed_admin_css', plugins_url( '/assets/css/admin-style.css', __FILE__ ));
    /*   Call css files */
    wp_enqueue_style( 'irfeed_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'irfeed_load_scripts_admin' );





require 'assets/lib/updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://irfeed.co.il/updates/plugin.json',
	__FILE__, //Full path to the main plugin file or functions.php.
	'irfeed'
);