<?php



/** Step 1. */
function irfeed_api_manage() {
    // add_options_page( 'Manage Stock Api', 'Stocks Api', 'manage_options', 'my-unique-identifier', 'irfeed_irfeed_api_params' );
    add_submenu_page( "options-general.php",  // Which menu parent
        "IRfeed",            // Page title
        "IRfeed",            // Menu title
        "manage_options",       // Minimum capability (manage_options is an easy way to target administrators)
        "IRfeed",            // Menu slug
        "irfeed_api_params"     // Callback that prints the markup
    );
}
/** Step 2 (from text above). */
add_action( 'admin_menu', 'irfeed_api_manage' );


function irfeed_api_params() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    ?>

<h1>IRfeed V2.0.0</h1>
<br>

<h2 class="nav-tab-wrapper">
	<a href="#" class="nav-tab nav-tab-active"><?php _e("Settings", "irfeed"); ?></a>
	<a href="/wp-admin/options-general.php?page=IRfeed-Quotes" class="nav-tab"><?php _e("Quotes", "irfeed"); ?></a>
	<a href="/wp-admin/options-general.php?page=IRfeed-Charts" class="nav-tab"><?php _e("Charts", "irfeed"); ?></a>
</h2>
<br>
<h3><?php _e("Welcome to the IRfeed Wordpress Plugin", "irfeed"); ?></h3>
<p><?php _e("Enter the license key & company number:", "irfeed"); ?></p>

<div class="card"> 
    
    <form method="post" action="<?php echo admin_url( 'admin-post.php'); ?>">
                    <input type="hidden" name="action" value="update_api_settings" />
                    <div class='table-row'>
                        <div class='table-left'>
                            <label><?php _e("Api Key:", "irfeed"); ?></label>
                        </div>

                        <div class='table-right'>
                            <input class="api-box " type="text" name="api_key" value="<?php echo get_option('api_key'); ?>" />
                            <label style="color:red";><?php if(!empty($_GET['api_key_error']))_e($_GET['api_key_error'], "irfeed") ?>
                        </div>
                    </div>
                    <div class='table-row'>
                        <div class='table-left'>
                            <label><?php _e("Security number:", "irfeed"); ?></label>
                        </div>

                        <div class='table-right'>
                            <input class="comp-box " type="text" name="companies_number" value="<?php echo get_option('companies_number'); ?>" />
                            <label class="alert"><?php if(!empty($_GET['companies_number_error']))_e($_GET['companies_number_error'], "irfeed") ?></label>
                        </div>
                    </div>

                    <div class='table-row-last'>
                        <label class="warning"><?php _e("Please separate a company number with a comma. For example: 9115, 2852, 6542", "irfeed"); ?></label>
                    </div>

                    <div class='table-row-last'>
                        <input class="button button-primary" type="submit" value="<?php _e("Save", "irfeed"); ?>" />
                    </div>
</form>


                        <?php
    if ( isset($_GET['status']) && $_GET['status']=='success') {
        ?>
        <div id="message" class="updated notice is-dismissible">
            <p><?php _e("Updated!", "irfeed"); ?></p>
            <button type="button" class="notice-dismiss">
                <span class="screen-reader-text"><?php _e("Dismiss this notice.", "irfeed"); ?></span>
            </button>
        </div>
    
</div>




        <?php
    }
}



function irfeed_api_handle_save() {
    // Get the options that were sent
    $api_key = (!empty($_POST["api_key"])) ? $_POST["api_key"] : NULL;
    $companies_number = (!empty($_POST["companies_number"])) ? $_POST["companies_number"] : NULL;

    // Validation would go here
    irfeed_validation($api_key, $companies_number);

    // Update the values
    update_option( "api_key", $api_key, TRUE );
    update_option("companies_number", $companies_number, TRUE);

    // Redirect back to settings page
    // The ?page=github corresponds to the "slug"
    // set in the fourth parameter of add_submenu_page() above.
    $redirect_url = get_bloginfo("url") . "/wp-admin/options-general.php?page=IRfeed&status=success";
    header("Location: ".$redirect_url);
    exit;
}
add_action( 'admin_post_update_api_settings', 'irfeed_api_handle_save' );

function irfeed_validation($api_key, $companies_number){
    $api_key_error = null;
    $companies_number_error = null;

    if(empty($api_key)){
        $api_key_error = 'Please fill this field';
    }

    if(empty($companies_number)){
        $companies_number_error = 'Please fill this field';
    }else{

        $companies_arr = explode(",", $companies_number);

        foreach($companies_arr as $company_number){
            if(!is_numeric($company_number)){
                $companies_number_error = 'Company number must contain only digits';
            }
        }
    }

    if(!empty($api_key_error) || !empty($companies_number_error)){
        $redirect_url = get_bloginfo("url") . "/wp-admin/options-general.php?page=IRfeed&api_key_error=".$api_key_error."&companies_number_error=".$companies_number_error;
        header("Location: ".$redirect_url);
        exit;
    }
}



function irfeed_second_tab() {
    // add_options_page( 'Manage Stock Api', 'Stocks Api', 'manage_options', 'my-unique-identifier', 'irfeed_api_params' );
    add_submenu_page( "options-general.php",  // Which menu parent
        "IRfeed-Quotes",            // Page title
        "IRfeed-Quotes",            // Menu title
        "manage_options",       // Minimum capability (manage_options is an easy way to target administrators)
        "IRfeed-Quotes",            // Menu slug
        "irfeed_second_tabPage"     // Callback that prints the markup
    );
}
/** Step 2 (from text above). */
add_action( 'admin_menu', 'irfeed_second_tab' );


function irfeed_second_tabPage() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    ?>
<h1>IRfeed V2.0.0</h1>
<br>

<h2 class="nav-tab-wrapper">
	<a href="/wp-admin/options-general.php?page=IRfeed" class="nav-tab"><?php _e("Settings", "irfeed"); ?></a>
	<a href="#" class="nav-tab nav-tab-active"><?php _e("Quotes", "irfeed"); ?></a>
	<a href="/wp-admin/options-general.php?page=IRfeed-Charts" class="nav-tab"><?php _e("Charts", "irfeed"); ?></a>
</h2>
   

<div class="wrap">

	<div id="col-container">

		<div id="col-right">
			<div class="col-wrap">
				<div class="inside">
				</div>
			</div>
			<!-- /col-wrap -->
		</div>
		<!-- /col-right -->

		<div id="col-left">

			<div class="col-wrap">
				<div class="inside">

<h3><?php esc_attr_e( 'Stock Quotes', 'irfeed' ); ?></h3>                    
<p><?php _e("Each value can be retrieved individually using a shortcode", "irfeed"); ?></p>              
<table class="widefat">
	<thead>
        <tr>
            <th class="row-title"><?php esc_attr_e( 'Value', 'irfeed' ); ?></th>
            <th><?php esc_attr_e( 'Shortcode', 'irfeed' ); ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="row-title"><label for="tablecell">rate</label></td>
            <td><code>[rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">base_rate</label></td>
            <td><code>[base_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">base_rate_change_percentage</label></td>
            <td><code>[base_rate_change_percentage stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">base_rate_change</label></td>
            <td><code>[base_rate_change stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">daily_highest_rate</label></td>
            <td><code>[daily_highest_rate stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">daily_lowest_rate</label></td>
            <td><code>[daily_lowest_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">heb_name</label></td>
            <td><code>[heb_name stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">eng_name</label></td>
            <td><code>[eng_name stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">heb_symbol</label></td>
            <td><code>[heb_symbol stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">eng_symbol</label></td>
            <td><code>[eng_symbol stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">exchange</label></td>
            <td><code>[exchange stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">symbol</label></td>
            <td><code>[symbol stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">all_year_maximum_rate</label></td>
            <td><code>[all_year_maximum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">all_year_minimum_rate</label></td>
            <td><code>[all_year_minimum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">last_deal_date</label></td>
            <td><code>[last_deal_date stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">last_deal_rate</label></td>
            <td><code>[last_deal_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">gross_yield_to_maturity</label></td>
            <td><code>[gross_yield_to_maturity stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">gross_duration</label></td>
            <td><code>[gross_duration stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">listed_fortune</label></td>
            <td><code>[listed_fortune stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">last_known_rate</label></td>
            <td><code>[last_known_rate stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">last_Known_Rate_date</label></td>
            <td><code>[last_Known_Rate_date stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">last_Known_Rate_time</label></td>
            <td><code>[last_Known_Rate_time stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">daily_turnover</label></td>
            <td><code>[daily_turnover stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">duality</label></td>
            <td><code>[duality stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">company_num</label></td>
            <td><code>[company_num stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">corp_num</label></td>
            <td><code>[corp_num stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">company_market_value</label></td>
            <td><code>[company_market_value stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">market_worth</label></td>
            <td><code>[market_worth stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">currency_code</label></td>
            <td><code>[currency_code stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">trading_stage</label></td>
            <td><code>[trading_stage stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">all_month_minimum_rate</label></td>
            <td><code>[all_month_minimum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">all_month_maximum_rate</label></td>
            <td><code>[all_month_maximum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">all_month_average_nis</label></td>
            <td><code>[all_month_average_nis stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">quarter_minimum_rate</label></td>
            <td><code>[quarter_minimum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">quarter_maximum_rate</label></td>
            <td><code>[quarter_maximum_rate stock='199018']</code></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell">quarter_average_nis</label></td>
            <td><code>[quarter_average_nis stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">up_to_one_year_minimum_rate</label></td>
            <td><code>[up_to_one_year_minimum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">up_to_one_year_maximum_rate</label></td>
            <td><code>[up_to_one_year_maximum_rate stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">up_to_one_year_average_nis</label></td>
            <td><code>[up_to_one_year_average_nis stock='199018']</code></td>
        </tr>

        <tr>
            <td class="row-title"><label for="tablecell">ratall_year_average_nise</label></td>
            <td><code>[all_year_average_nis stock='199018']</code></td>
        </tr>
    
	</tbody>
	<tfoot>
	<tr>
		<th class="row-title"><?php esc_attr_e( 'Value', 'irfeed' ); ?></th>
		<th><?php esc_attr_e( 'Shortcode', 'irfeed' ); ?></th>
	</tr>
	</tfoot>
</table>
                    
                    
				</div>
			</div>
			<!-- /col-wrap -->

		</div>
		<!-- /col-left -->

	</div>
	<!-- /col-container -->

</div> <!-- .wrap -->   
    
    
    



    <?php if ( isset($_GET['status']) && $_GET['status']=='success') { ?>
    <?php
    }
}

function irfeed_third_tab() {
    // add_options_page( 'Manage Stock Api', 'Stocks Api', 'manage_options', 'my-unique-identifier', 'irfeed_api_params' );
    add_submenu_page( "options-general.php",  // Which menu parent
        "IRfeed-Charts",            // Page title
        "IRfeed-Charts",            // Menu title
        "manage_options",       // Minimum capability (manage_options is an easy way to target administrators)
        "IRfeed-Charts",            // Menu slug
        "irfeed_third_tabPage"     // Callback that prints the markup
    );
}
/** Step 2 (from text above). */
add_action( 'admin_menu', 'irfeed_third_tab' );


function irfeed_third_tabPage() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    ?>
<h1>IRfeed V2.0.0</h1>
<br>

<h2 class="nav-tab-wrapper">
	<a href="/wp-admin/options-general.php?page=IRfeed" class="nav-tab"><?php _e("Settings", "irfeed"); ?></a>
	<a href="/wp-admin/options-general.php?page=IRfeed-Quotes" class="nav-tab"><?php _e("Quotes", "irfeed"); ?></a>
	<a href="#" class="nav-tab  nav-tab-active"><?php _e("Charts", "irfeed"); ?></a>
</h2>
   

<div class="wrap">

	<div id="col-container">

		<div id="col-right">
			<div class="col-wrap">
				<div class="inside">

				</div>
			</div>
			<!-- /col-wrap -->
		</div>
		<!-- /col-right -->

		<div id="col-left">

			<div class="col-wrap">
				<div class="inside">
                    <h3><?php esc_attr_e( 'Stock Charts', 'irfeed' ); ?></h3>                    
                    <p><?php _e("Each value can be retrieved individually using a shortcode", "irfeed"); ?></p>              
                    <table class="widefat">
                        <thead>
                            <tr>
                                <th class="row-title"><?php esc_attr_e( 'Value', 'irfeed' ); ?></th>
                                <th><?php esc_attr_e( 'Shortcode', 'irfeed' ); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="row-title"><label for="tablecell">Standart Chart</label></td>
                                <td><code>[dicechart stock='199018']</code></td>
                            </tr>

                            <tr>
                                <td class="row-title"><label for="tablecell">Mini Chart (Day)</label></td>
                                <td><code>[dicechart_mini_day stock='199018']</code></td>
                            </tr>

                            <tr>
                                <td class="row-title"><label for="tablecell">Mini Chart (Month)</label></td>
                                <td><code>[dicechart_mini_month stock='199018']</code></td>
                            </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="row-title"><?php esc_attr_e( 'Value', 'irfeed' ); ?></th>
                            <th><?php esc_attr_e( 'Shortcode', 'irfeed' ); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    
                 <br>
                    <p><?php esc_attr_e( 'List of design variables:', 'irfeed' ); ?></p>  
                    <table class="widefat">
                        <thead>
                            <tr>
                                <th class="row-title"><?php esc_attr_e( 'Name', 'irfeed' ); ?></th>
                                <th><?php esc_attr_e( 'Value', 'irfeed' ); ?></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td class="row-title"><label for="tablecell">Mode</label></td>
                                 <td>
                                    <code>mode='light'</code>
                                    <br>
                                    <code>mode='dark'</code>
                                </td>
                            </tr>

                            <tr>
                                <td class="row-title"><label for="tablecell">Type</label></td>
                                <td>
                                    <code>type='area'</code>
                                    <br>
                                    <code>type='line'</code>
                                </td>
                            </tr>

                            <tr>
                                <td class="row-title"><label for="tablecell">Height (px)</label></td>
                                <td><code>height='350'</code></td>
                            </tr>

                            <tr>
                                <td class="row-title"><label for="tablecell">Chart Color (hex)</label></td>
                                <td><code>color='#3e75a3'</code></td>
                            </tr>

                            <tr>
                                <td class="row-title"><label for="tablecell">Price Color (hex)</label></td>
                                <td><code>colorbox='#3e75a3'</code></td>
                            </tr>
                                
                            <tr>
                                <td class="row-title"><label for="tablecell">Line thickness</label></td>
                                <td><code>stroke='2'</code></td>
                            </tr>
                                
                            <tr>
                                <td class="row-title"><label for="tablecell">Marker size (point)</label></td>
                                <td><code>markersize='4'</code></td>
                            </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="row-title"><?php esc_attr_e( 'Name', 'irfeed' ); ?></th>
                            <th><?php esc_attr_e( 'Value', 'irfeed' ); ?></th>
                        </tr>
                        </tfoot>
                    </table>
				</div>
			</div>
			<!-- /col-wrap -->

		</div>
		<!-- /col-left -->

	</div>
	<!-- /col-container -->

</div> <!-- .wrap -->   
    
    
    



    <?php if ( isset($_GET['status']) && $_GET['status']=='success') { ?>
    <?php
    }
}

function irfeed_remove_menu_items(){
   remove_submenu_page( 'options-general.php', 'IRfeed-Quotes' );
   remove_submenu_page( 'options-general.php', 'IRfeed-Charts' );
}

add_action( 'admin_menu', 'irfeed_remove_menu_items', 999 );
?>